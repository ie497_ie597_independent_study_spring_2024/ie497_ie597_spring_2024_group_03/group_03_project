g++ torch_test.cpp -o torch_example -std=c++17 \
-I/groupstorage/group01/includes/TorchIncludes/libtorch/include \
-I/groupstorage/group01/includes/TorchIncludes/libtorch/include/torch/csrc/api/include \
-L/groupstorage/group01/includes/TorchIncludes/libtorch/lib \
-ltorch -ltorch_cpu -lc10 -lpthread -Wl,-rpath,/groupstorage/group01/includes/TorchIncludes/libtorch/lib

./torch_example
