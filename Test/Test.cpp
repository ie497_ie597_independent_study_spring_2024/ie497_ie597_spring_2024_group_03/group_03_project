// Refer to the header files for the explanation of the functions in detail
// In VSCode, hovering your mouse above the function renders the explanation of from the .h file as a pop up
#ifdef _WIN32
    #include "stdafx.h"
#endif

#include "Test.h"

using namespace RCM::StrategyStudio;
using namespace RCM::StrategyStudio::MarketModels;
using namespace RCM::StrategyStudio::Utilities;

using namespace std;

// Constructor to initialize member variables of the class to their initial values.
Test::Test(StrategyID strategyID,
                    const string& strategyName,
                    const string& groupName):
    Strategy(strategyID, strategyName, groupName),
    name("wrong strategy name lolll"),
    working("not working"),
    debug_trades(false),
    debug_bars(true),
    debug_quotes(false),
    debug_order_updates(false),
    debug_scheduled_events(false),
    debug_d(false)
    {}
// Destructor for class
Test::~Test(){}

void Test::DefineStrategyParams(){
    params().CreateParam(CreateStrategyParamArgs("name", STRATEGY_PARAM_TYPE_STARTUP, VALUE_TYPE_STRING, name));
    params().CreateParam(CreateStrategyParamArgs("working", STRATEGY_PARAM_TYPE_STARTUP, VALUE_TYPE_STRING, working));
    params().CreateParam(CreateStrategyParamArgs("debug_trades", STRATEGY_PARAM_TYPE_STARTUP, VALUE_TYPE_BOOL, debug_trades));
    params().CreateParam(CreateStrategyParamArgs("debug_bars", STRATEGY_PARAM_TYPE_STARTUP, VALUE_TYPE_BOOL, debug_bars));
    params().CreateParam(CreateStrategyParamArgs("debug_quotes", STRATEGY_PARAM_TYPE_STARTUP, VALUE_TYPE_BOOL, debug_quotes));
    params().CreateParam(CreateStrategyParamArgs("debug_order_updates", STRATEGY_PARAM_TYPE_STARTUP, VALUE_TYPE_BOOL, debug_order_updates));
    params().CreateParam(CreateStrategyParamArgs("debug_scheduled_events", STRATEGY_PARAM_TYPE_STARTUP, VALUE_TYPE_BOOL, debug_scheduled_events));
    params().CreateParam(CreateStrategyParamArgs("debug_d", STRATEGY_PARAM_TYPE_STARTUP, VALUE_TYPE_BOOL, debug_d));
}

void Test::DefineStrategyCommands(){}

// By default, SS will register to trades/quotes/depth data for the instruments you have requested via command_line.
void Test::RegisterForStrategyEvents(StrategyEventRegister* eventRegister, DateType currDate){
    for (SymbolSetConstIter it = symbols_begin(); it != symbols_end(); ++it) {
        eventRegister->RegisterForBars(*it, BAR_TYPE_TIME, 60);
        }
    eventRegister->RegisterForRecurringScheduledEvents("hourly ping", USEquityOpenUTCTime(currDate), USEquityCloseUTCTime(currDate), boost::posix_time::hours(1));
    eventRegister->RegisterForSingleScheduledEvent("day_end",USEquityCloseUTCTime(currDate)-boost::posix_time::seconds(10));
}

void Test::OnTrade(const TradeDataEventMsg& msg) {
    const Trade* trade = &msg.trade();
    const Instrument* instrument = &msg.instrument();
    bool debug_trades;
    if (params().GetParam("debug_trades")->Get(&debug_trades) && debug_trades){
    cout<< "Trade on " << instrument->symbol() << " at " << msg.event_time() << " at " << trade->price() << " for " << trade->size() << " at " << msg.side() << endl;
    cout << "bid is " << instrument->top_quote().bid() << " , ask is " << instrument->top_quote().ask() << endl;
    if (msg.IsConsolidated())
        cout << "Trade is from consolidated feed" << endl;
    else if (msg.IsDirect())
        cout << "Trade is from direct feed" << endl;
    else if (msg.IsFromDepth())
        cout << "Trade is from depth feed" << endl;
    //RecordAccountStats(instrument);
    }
}

void Test::OnScheduledEvent(const ScheduledEventMsg& msg) {
    bool debug_scheduled_events;
    if (params().GetParam("debug_scheduled_events")->Get(&debug_scheduled_events) && debug_scheduled_events)
        cout << "Scheduled event " << msg.scheduled_event_name() << " at " << msg.event_time() << endl;
}

void Test::OnOrderUpdate(const OrderUpdateEventMsg& msg) {
    bool debug_order_updates;
    if (params().GetParam("debug_order_updates")->Get(&debug_order_updates) && debug_order_updates){
        // Examples of other information that can be accessed through the msg object, but they are not used in this implementation.
        cout << "name = " << msg.name() << endl;
        cout << "order id = " << msg.order_id() << endl;
        cout << "fill occurred = " << msg.fill_occurred() << endl;
        cout << "update type = " << msg.update_type() << endl;

        // Update time of the order update event is printed to the console using std::cout.
        cout << "time " << msg.update_time() << endl;
    }
}

void Test::OnBar(const BarEventMsg& msg){
    // Uncomment below code to print bar statistics to the console
    const Bar& bar = msg.bar(); // Get the bar object from the BarEventMsg object
    const Instrument* instrument = &msg.instrument(); // Get the instrument object from the BarEventMsg object
    bool debug_bars;
    if ((bar.IsValid()) && (params().GetParam("debug_bars")->Get(&debug_bars) && debug_bars)){
        cout << msg.bar_time() << "bid:- " << msg.instrument().top_quote().bid() << ", ask:- " << msg.instrument().top_quote().ask() << endl;
        cout << msg.bar_time() << "high:- " << bar.high() << ", low:- " << bar.low() << ", open:- " << bar.open() << ", close:- " << bar.close() << endl;
        cout << msg.bar_time() << "volume:- " << bar.volume() << endl;
    }
}

void Test::OnQuote(const QuoteEventMsg& msg){
    bool debug_quotes;
    if (params().GetParam("debug_quotes")->Get(&debug_quotes) && debug_quotes){
    cout << msg.name() << endl;
    cout << "Quote on " << msg.instrument().symbol() << " at " << msg.event_time() << " bid " << msg.quote().bid() << " ask " << msg.quote().ask() << endl;
    cout << "bid size " << msg.quote().bid_size() << " ask size " << msg.quote().ask_size() << endl;
    cout << "Source time " << msg.source_time() << ", feed handler time " << msg.feed_handler_time() << " , Adapter_time" << endl;
    }
}

void Test::OnDepth(const MarketDepthEventMsg& msg){
    bool debug_d;
    if (params().GetParam("debug_d")->Get(&debug_d) && debug_d){
        cout << msg.name() << endl;
        cout << "Depth Update " << msg.instrument().symbol() << " at " << msg.event_time() <<  " at price " << msg.depth_update().price() << " for " << msg.depth_update().size() << " at " << msg.depth_update().side() << endl;
        cout << "Source time " << msg.source_time() << ", feed handler time " << msg.feed_handler_time() << " , Adapter_time" << endl;
    }
}

void Test::OnMarketState(const MarketStateEventMsg& msg){}

void Test::OnResetStrategyState(){}

void Test::OnParamChanged(StrategyParam& param) {
    cout << param.param_name() << " has changed to " << param.ToString() << endl;
}