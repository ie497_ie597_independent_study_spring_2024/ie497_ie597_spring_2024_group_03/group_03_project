## *********************************************************************************************************************
# Choose what you want to test by passing the key as an argument
# Usage example: python3 onetick_test.py trades 100
# This will run the test with the key 'trades' and a timeout of 100 seconds
# keys :- all_params, trades, bars, quotes, order_updates, scheduled_events, depth
# dont forget to change stock name in the makefile and the dates if needed. 

import subprocess
import sys
import time

def make_with_python(command, timeout=100):
    try:
        # Execute the command with check=True to automatically raise an exception for non-zero return codes
        subprocess.run(command, check=True, timeout=timeout)
        print("Command executed successfully.")
        return True  # Indicates success
    except:
        print("An error occurred.", sys.exc_info())
        return False

if __name__=="__main__":
    # Command to execute
    episode_parameters = "name=Test|working=working|debug_trades=false|debug_bars=false|debug_quotes=false|debug_order_updates=false|debug_scheduled_events=false|debug_d=false"
    start_date = "2023-10-29"
    end_date = "2023-10-31"
    param_combinations = {'all_params': 'name=Test_all|working=working|debug_trades=true|debug_bars=true|debug_quotes=true|debug_order_updates=true|debug_scheduled_events=true|debug_d=true',
                          'trades': 'name=Test_trades|working=working|debug_trades=true|debug_bars=false|debug_quotes=false|debug_order_updates=false|debug_scheduled_events=false|debug_d=false',
                          'bars': 'name=Test_bars|working=working|debug_trades=false|debug_bars=true|debug_quotes=false|debug_order_updates=false|debug_scheduled_events=false|debug_d=false',
                          'quotes': 'name=Test_quotes|working=working|debug_trades=false|debug_bars=false|debug_quotes=true|debug_order_updates=false|debug_scheduled_events=false|debug_d=false',
                          'order_updates': 'name=Test_order_updates|working=working|debug_trades=false|debug_bars=false|debug_quotes=false|debug_order_updates=true|debug_scheduled_events=false|debug_d=false',
                          'scheduled_events': 'name=Test_scheduled_events|working=working|debug_trades=false|debug_bars=false|debug_quotes=false|debug_order_updates=false|debug_scheduled_events=true|debug_d=false',
                          'depth': 'name=Test_depth|working=working|debug_trades=false|debug_bars=false|debug_quotes=false|debug_order_updates=false|debug_scheduled_events=false|debug_d=true'}
    if len(sys.argv) < 1:
        commands = [
            ["make", "start_server"],
            ["make", "create_instance"],
            ['make', 'edit_params', f'EPISODE_PARAMETERS={param_combinations["all_params"]}'],
            ["make", "run_backtest"]
        ]
    else:
        if sys.argv[1] not in param_combinations.keys():
            print(f"Invalid test key: {sys.argv[1]}")
            sys.exit(1)
        commands = [
            ["make", "start_server"],
            ["make", "create_instance"],
            ['make', 'edit_params', f'EPISODE_PARAMETERS={param_combinations[sys.argv[1]]}'],
            ["make", "run_backtest"]
        ]
    # Execute the commands, stopping if one fails
    for command in commands:
        success = make_with_python(command, int(sys.argv[2]))
        if not success:
            sys.exit(1)  # Terminate the program if a command fails
        time.sleep(5)