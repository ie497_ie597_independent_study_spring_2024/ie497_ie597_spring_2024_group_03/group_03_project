#include <iostream>
#include <sqlite_modern_cpp.h>
#include <filesystem>

int main() {
    try {
        // Create or open the SQLite database file
        std::string db_path = "/groupstorage/group01/Test/dbs/test.db";
        std::unique_ptr<sqlite::database> db;
        auto directory = std::filesystem::path().parent_path();

        if (!directory.empty() && !std::filesystem::exists(directory)) {
            std::filesystem::create_directories(directory);
        }

        db = std::make_unique<sqlite::database>(db_path);

        // Create a table for storing users
        (*db) << "CREATE TABLE IF NOT EXISTS users ("
                 "id INTEGER PRIMARY KEY AUTOINCREMENT, "
                 "username TEXT NOT NULL, "
                 "email TEXT NOT NULL);";

        // Insert some sample user data
        (*db) << "INSERT INTO users (username, email) VALUES (?, ?);" 
              << "john_doe" << "john@example.com";
        (*db) << "INSERT INTO users (username, email) VALUES (?, ?);" 
              << "jane_smith" << "jane@example.com";

        // Create a table for storing products
        (*db) << "CREATE TABLE IF NOT EXISTS products ("
              "id INTEGER PRIMARY KEY AUTOINCREMENT, "
              "name TEXT NOT NULL, "
              "price REAL NOT NULL);";

        // Insert some sample product data
        (*db) << "INSERT INTO products (name, price) VALUES (?, ?);" 
              << "Widget" << 9.99;
        (*db) << "INSERT INTO products (name, price) VALUES (?, ?);" 
              << "Gadget" << 19.99;

        std::cout << "Database created successfully!" << std::endl;
        int count = 0;
        (*db) << "SELECT COUNT(*) FROM users;" >> count;
        std::cout << "Number of rows in the table users: " << count << std::endl;
        (*db) << "SELECT COUNT(*) FROM products;" >> count;
        std::cout << "Number of rows in the table products: " << count << std::endl;
        
    } catch (const sqlite::sqlite_exception& e) {
        std::cerr << "SQLite error: " << e.what() << std::endl;
        return 1;
    } catch (const std::exception& e) {
        std::cerr << "Error: " << e.what() << std::endl;
        return 1;
    }
    return 0;
}