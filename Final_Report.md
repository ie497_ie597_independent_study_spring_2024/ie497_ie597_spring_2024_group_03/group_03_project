# Final Report for IE597 Independent Study UIUC

Authored by: Samanvay Malapally Sudhakara [MSFE '24, UIUC] [linkedin](https://www.linkedin.com/in/samanvay-malapally-sudhakara-148836212/)

## Projects on Repository

1. [Project 1: Guide to Running Projects with Strategy Studio](StrategyStudioGuide.md)
2. [Project 2: Development and Implementation of a Reinforcement Learning Based Agents in High Frequency Trading Using Strategy Studio for Backtesting](RLTrading.md)

## Comprehensive Project Work Summary

1. Project 1
    - rewrote most provision scripts for the projects in bash script to allow for easy use of the `StrategyCommandLine` without having to manually enter the commands.
    - provision scripts now generalise across all projects and can be used to run any project with changes being made only to the respective makefile.
    - wrote test scripts for other teams to test their data access and also erve as a guide for strategy messages and calling related methods.
    - modfied to the makefile to accomodate for the new provision scripts and also gave more control to the user to run the project with the desired parameters.
    - rewrote an entire result analytics pipeline to seamlessly capture the respective .CRA files, process them and use the generated .csv file to generate all sorts of metrics such as Sharpe Ratio, Sortino Ratio, Max Drawdown, etc and also capture the commit id and the respective parameters used to run the project and stores it in a json file. It also generates a vast array of graphs if the user desires to do so.
    - based on the rewritten scripts we can now easily control the entire project using a few `run.subprocess` commands and the entire project can be run in a single python script.
    - used the above python integration to build an optimisation script that uses Optuna to optimise the parameters of the project and run the project with the optimised parameters and generate results.

2. Project 2
    - built a Deep Q Network and a Deep Deterministic Policy Gradient Agent in C++ using libTorch and integrated it with Strategy Studio.
    - the script also saves the model for evaluation or futher training.
    - used sqlite3 to store data and retrieve data from the database while also calling it using in a C++ program.
    - wrote a trainer script to train the agent and evaluate the agent using the Strategy Studio.
    - can run multiple agents on multiple instruments at the same time.

## Reflections,Learnings and Challenges from the Independent Study

1. Project 1
    - The project was a good lesson on bash scripting and skills involving linking multiple files together.
    - It also helped me explore an Advanced Strategy like Strategy Studio on a Fundamental Level and get a better understanding on building and working with backtesting frameworks.
    - Startegy Studio does not have proper error messages and debugging tools which makes it hard to debug the code. This does impede the strategy building building process but is a good learning experience and prepares you for real world scenarios where you might not have the luxury of debugging tools.

2. Project 2
    - The project was a good lesson on how to build complex Reinforcement Learning based agent in C++ and how to integrate it with Strategy Studio.
    - Learnt how to build entire neural networks in C++ instead of python using libTorch.
    - Using sqlite3 to store data and retrieve data from the database while also calling it using in a C++ program.
    - The lack of example code and the issues with the lack of proper debug logs in Strategy Studio made it harder to debug the code as there were too many interlinked modules and points of failure.
    - This was a good learning experience and helped me understand abstraction, pointers and Object oriented programming at a deeper level.
