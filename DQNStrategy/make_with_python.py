import subprocess
import sys
import time

def make_with_python(command, timeout=100):
    try:
        # Execute the command with check=True to automatically raise an exception for non-zero return codes
        subprocess.run(command, check=True, timeout=timeout)
        print("Command executed successfully.")
        return True  # Indicates success
    except:
        print("An error occurred.", sys.exc_info())
        return False

if __name__=="__main__":
    wait_time = sys.argv[2] 
    commands = [
        ["make", "start_server"],
        ["make", "create_instance"],
        ["make", "run_backtest", f"WAIT_TIME={wait_time}"]
    ]
    
    for command in commands:
        success = make_with_python(command, int(sys.argv[1]))
        if not success:
            sys.exit(1)  # Terminate the program if a command fails
        time.sleep(5)