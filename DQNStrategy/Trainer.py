import subprocess
import sys
import time
import os
from datetime import datetime, timedelta

def make_with_python(command, timeout=100):
    try:
        # Execute the command with check=True to automatically raise an exception for non-zero return codes
        subprocess.run(command, check=True, timeout=timeout)
        print("Command executed successfully.")
        return True  # Indicates success
    except:
        print("An error occurred.", sys.exc_info())
        return False

def run_commands(commands, timeout=100):
    for command in commands:
        success = make_with_python(command, timeout)
        if not success:
            sys.exit(1)  # Terminate the program if a command fails
        time.sleep(1)

def break_into_continuous_ranges(dates, start_date, end_date):
    # Convert string dates to datetime objects
    dates = [datetime.strptime(date, '%Y-%m-%d') for date in dates]
    start_date = datetime.strptime(start_date, '%Y-%m-%d')
    end_date = datetime.strptime(end_date, '%Y-%m-%d')
    
    # Filter dates within the specified range
    dates = sorted([date for date in dates if start_date <= date <= end_date])
    if not dates:
        return []
    
    # Find continuous date ranges
    continuous_ranges = []
    current_range = [dates[0]]

    for i in range(1, len(dates)):
        if dates[i] == dates[i-1] + timedelta(days=1):
            current_range.append(dates[i])
        else:
            continuous_ranges.append((current_range[0], current_range[-1]))
            current_range = [dates[i]]
    continuous_ranges.append((current_range[0], current_range[-1]))
    # Convert datetime objects back to string
    continuous_ranges = [[date.strftime('%Y-%m-%d') for date in date_range] for date_range in continuous_ranges]
    
    return continuous_ranges
        
def get_available_date_ranges(directory, ticker="GOOG", train_start_date="2023-01-01", train_end_date="2023-01-07"):
    files = os.listdir(directory)
    dates = [str(file.split("_")[2]) for file in files if file.startswith("tick_"+ticker)]
    dates = [date.split(".")[0] for date in dates]
    dates = [date[:4]+"-"+date[4:6]+"-"+date[6:] for date in dates]
    ranges = break_into_continuous_ranges(dates, train_start_date, train_end_date)
    return ranges
    
def get_one_run(ranges, initial = True):
    if  not initial:
        one_run = [["make", "run_backtest", f"START_DATE={ranges[0][0]}", f"END_DATE={ranges[0][1]}"]]
    else:
        one_run = []
    for range in ranges[1:]:
        one_run += [["make", "run_backtest", f"START_DATE={range[0]}", f"END_DATE={range[1]}"]]
    return one_run
    
def train(symbol,parameters, episodes=2, train_start_date="2023-10-31", train_end_date="2023-10-31"):
    initial_parameters = parameters+"|new_model=true|eval=false"
    training_parameters = parameters+"|new_model=false|eval=false"
    date_ranges = get_available_date_ranges("/iex/data/text_tick_data/", symbol, train_start_date, train_end_date)
    commands = [
        ["make", "start_server"],
        ["make", "create_instance", f"PARAMETERS={initial_parameters}", f"SYMBOLS={symbol}"],
        ["make", "run_backtest", f"START_DATE={date_ranges[0][0]}", f"END_DATE={date_ranges[0][1]}"],
        ["make", "edit_params", f"EPISODE_PARAMETERS={training_parameters}"]
    ]
    initial_run = get_one_run(date_ranges, True)
    commands += initial_run
    other_runs = get_one_run(date_ranges, False)
    commands += other_runs*(episodes-1)
    run_commands(commands)
    
def eval_(symbol, parameters, eval_start_date="2023-11-30", eval_end_date="2023-11-30"):
    eval_parameters = parameters+"|new_model=false|eval=true"
    commands = [
        ["make", "start_server"],
        ["make", "create_instance", f"PARAMETERS={eval_parameters}", f"SYMBOLS={symbol}"],
        ["make", "run_backtest_with_results", f"START_DATE={eval_start_date}", f"END_DATE={eval_end_date}"]
    ]
    run_commands(commands)
    
if __name__=="__main__":
    task = sys.argv[1]
    # train loop 
    symbol = "GOOG"
    train_start_date = "2023-01-01"
    train_end_date = "2023-01-15"
    eval_start_date = "2023-01-08"
    eval_end_date = "2023-01-08"
    parameters = "epsilon=1.0|volume_clip=20.0|epsilon_decay=0.00001|epsilon_end=0.01|window_size=10|normalise_factor=3.0|normalise_window_size=10|vol_window_size=20|n_parameters=4|memory_size=10000|batch_size=64|gamma=0.99|target_update_frequency=1000|bar_interval=60|lot_size=1"
    if task == "train":
        episodes = int(sys.argv[2])
        train(symbol,parameters, episodes, train_start_date, train_end_date)
    elif task == "eval":
        eval_(symbol,parameters, eval_start_date, eval_end_date)
    else:
        episodes = int(sys.argv[2])
        train(symbol,parameters, episodes, train_start_date, train_end_date)
        eval_(symbol,parameters, eval_start_date, eval_end_date)