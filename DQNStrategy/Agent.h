#include <torch/torch.h>
#include <vector>
#include <string>
#include "DQN.h"
#include <memory>
#include <random>
#include <algorithm>
#include <filesystem>
#include "experience.h"

class DQNAgent {

private:
    std::shared_ptr<Net> dqn; // DQN network
    std::shared_ptr<Net> target_dqn; // Target network
    std::shared_ptr<torch::optim::Adam> optimizer; // Optimizer (pointer to support lazy initialization)
    double gamma; // Discount factor
    int target_update_frequency; // Frequency of target network updates
    int steps; // Number of steps taken
    std::string instrument_name;

public:
    // Default constructor
    DQNAgent()
        : dqn(std::make_shared<Net>(1, 1)), // Initialize with dummy Net
          target_dqn(std::make_shared<Net>(1, 1)), // Initialize with dummy Net
          gamma(0.98),
          target_update_frequency(1000),
          instrument_name("default"),
          steps(0) {
        std::cout << "Default DQNAgent initialized" << std::endl;
    }

    // Parameterized constructor
    DQNAgent(int state_dim, int action_dim, double gamma = 0.99, int target_update_frequency = 1000,std::string instrument_name = " " ,bool new_model = true)
        : dqn(std::make_shared<Net>(state_dim, action_dim)),
          target_dqn(std::make_shared<Net>(state_dim, action_dim)),
          gamma(gamma),
          target_update_frequency(target_update_frequency),
          instrument_name(instrument_name),
          steps(0) {
        if (!new_model) {
            std::cout << "Loading existing model..." << endl;
            load_agent();
        }else{
            initialize_optimizer();
        }
        update_target_network();
        std::cout << "DQNAgent initialized" << std::endl;
    }

    // Delete the copy constructor
    DQNAgent(const DQNAgent&) = delete;

    // Delete the copy assignment operator
    DQNAgent& operator=(const DQNAgent&) = delete;

    // Move constructor
    DQNAgent(DQNAgent&& other) noexcept
        : dqn(std::move(other.dqn)),
          target_dqn(std::move(other.target_dqn)),
          optimizer(std::move(other.optimizer)),
          gamma(other.gamma),
          target_update_frequency(other.target_update_frequency),
          instrument_name(other.instrument_name),
          steps(other.steps) {
        std::cout << "DQNAgent moved" << std::endl;
    }

    // Move assignment operator
    DQNAgent& operator=(DQNAgent&& other) noexcept {
        if (this != &other) {
            dqn = std::move(other.dqn);
            target_dqn = std::move(other.target_dqn);
            optimizer = std::move(other.optimizer);
            gamma = other.gamma;
            target_update_frequency = other.target_update_frequency;
            instrument_name = other.instrument_name;
            steps = other.steps;
        }
        return *this;
    }

    void initialize_optimizer(double lr = 0.001) {
        optimizer = std::make_shared<torch::optim::Adam>(dqn->parameters(), torch::optim::AdamOptions(lr));
    }

    void update_target_network() {
        auto source_params = dqn->parameters();
        auto target_params = target_dqn->parameters();

        if (source_params.size() != target_params.size()) {
            throw std::runtime_error("Size mismatch between source and target network parameters");
        }
        for (size_t i = 0; i < source_params.size(); ++i) {
            target_params[i].data().copy_(source_params[i].data());
        }
        std::cout << "Target network updated" << std::endl;
    }

    int get_action(vector<double> state_vector) {
        vector <float> state_float = convertToFloat(state_vector);
        torch::Tensor state = torch::from_blob(state_float.data(), {(int)state_float.size()}, torch::kFloat32);
        dqn->eval();
        torch::Tensor action_tensor = torch::argmax(dqn->forward(state.unsqueeze(0)), 1);
        return action_tensor.item<int>();
    }

    void learn(std::vector<Experience> batch) {
        if (!optimizer) {
            initialize_optimizer();
        }
        // Extract states, actions, rewards, etc.
        std::vector<torch::Tensor> states, actions, rewards, next_states, dones;

        // std::vector<double> demo_float = {0.3,0.6,0.9};
        // torch::Tensor demo_tensor = torch::from_blob(demo_float.data(), {3}, torch::kFloat32);
        // cout << "Demo tensor" << demo_tensor << endl;

        for (const auto &exp : batch) {
            // Unpack the experience tuple
            vector<double> state = std::get<0>(exp);
            int action = std::get<1>(exp);
            double reward = std::get<2>(exp);
            vector<double> next_state = std::get<3>(exp);
            int done = std::get<4>(exp);

            // Convert std::vector<double> to std::vector<float> and then to torch::Tensor
            auto state_floats = convertToFloat(state);
            auto next_state_floats = convertToFloat(next_state);

            torch::Tensor state_tensor = torch::from_blob(state_floats.data(), {(int)state_floats.size()}, torch::kFloat32).clone();
            states.push_back(state_tensor);
            actions.push_back(torch::tensor({action}, torch::kInt64));
            rewards.push_back(torch::tensor({reward}, torch::kFloat32));
            torch::Tensor next_state_tensor = torch::from_blob(next_state_floats.data(), {(int)next_state_floats.size()}, torch::kFloat32).clone();
            next_states.push_back(next_state_tensor);
            dones.push_back(torch::tensor({done}, torch::kInt64));
        }

        // Stack tensors to form batches
        torch::Tensor state_batch = torch::stack(states);
        torch::Tensor action_batch = torch::stack(actions).squeeze();
        torch::Tensor reward_batch = torch::stack(rewards).squeeze();
        torch::Tensor next_state_batch = torch::stack(next_states);
        torch::Tensor done_batch = torch::stack(dones).squeeze();

        try {
            // Activate training mode
            dqn->train();
            // Compute current Q-values
            torch::Tensor q_values = dqn->forward(state_batch).gather(1, action_batch.unsqueeze(1)).squeeze(-1);

            // Compute next Q-values from target network
            torch::Tensor next_state_values = target_dqn->forward(next_state_batch);
            torch::Tensor next_q_values = std::get<0>(next_state_values.max(1));

            // Compute expected Q-values for the non-terminal states
            torch::Tensor expected_q_values = reward_batch + gamma * next_q_values * (1 - done_batch);

            // Compute loss
            torch::Tensor loss = torch::nn::functional::mse_loss(q_values, expected_q_values);
            // Print the loss value
            // std::cout << "Loss: " << loss.item<float>() << std::endl;
            optimizer->zero_grad();
            loss.backward();
            optimizer->step();
        } catch (const std::exception& e) {
            std::cerr << "Error : " << e.what() << std::endl;
            throw e;
        }

        // Update target network
        steps += 1;
        if (steps % target_update_frequency == 0) {
            update_target_network();
        }
    }

    std::vector<float> convertToFloat(const std::vector<double>& input) {
        std::vector<float> output(input.size());
        std::transform(input.begin(), input.end(), output.begin(), [](double val) {
            return static_cast<float>(val);
        });
        return output;
    }

    void save_agent(){
        save_model(dqn, optimizer);
    }

    void load_agent(){
        load_model(dqn, optimizer);
    }

    void print_agent_model_summary() {
        print_model_summary(*dqn);
    }

private:
    void save_model(const std::shared_ptr<Net>& model, const std::shared_ptr<torch::optim::Adam>& optimizer) {
        std::string directory = "/groupstorage/group01/DQNStrategy/models/";
        check_and_create_directory(directory); // Ensure the directory exists or create it

        std::string model_path = directory + instrument_name + "_model.pt";
        std::string optimizer_path = directory + instrument_name + "_optimizer.pt";

        try {
            // Save the model
            torch::save(model, model_path);
            std::cout << "Model saved to " << model_path << std::endl;

            // Save the optimizer
            torch::save(*optimizer, optimizer_path);
            std::cout << "Optimizer saved to " << optimizer_path << std::endl;

        } catch (const std::exception& e) {
            std::cerr << "Error saving model or optimizer: " << e.what() << std::endl;
        }
    }

    void load_model(const std::shared_ptr<Net>& model, const std::shared_ptr<torch::optim::Adam>& optimizer) {
        std::string directory = "/groupstorage/group01/DQNStrategy/models/";

        std::string model_path = directory + instrument_name + "_model.pt";
        std::string optimizer_path = directory + instrument_name + "_optimizer.pt";

        try {
            if (check_model_exists(model_path)) {
                std::cout << "Loading model..." << std::endl;
                torch::load(model, model_path);
                std::cout << "Model loaded successfully." << std::endl;
            } else {
                std::cerr << "Model file not found." << std::endl;
                return;
            }

            // if (check_model_exists(optimizer_path)) {
            //     std::cout << "Loading optimizer..." << std::endl;
            //     torch::load(*optimizer, optimizer_path);
            //     std::cout << "Optimizer loaded successfully." << std::endl;
            // } else {
            //     std::cerr << "Optimizer file not found." << std::endl;
            //     // Handle optimizer not found - you may want to recreate the optimizer
            //     initialize_optimizer(); // Optional
            // }
            //testing with new optimizer
            initialize_optimizer();
        } catch (const std::exception& e) {
            std::cerr << "Error loading model or optimizer: " << e.what() << std::endl;
            // Handle error and potentially recreate optimizer
        }
    }

    void check_and_create_directory(const std::string& path) {
        std::filesystem::path dir_path(path);

        // Check if the directory exists
        if (std::filesystem::exists(dir_path)) {
            std::cout << "Directory already exists: " << path << std::endl;
        } else {
            // Create the directory
            if (std::filesystem::create_directories(dir_path)) {
                std::cout << "Directory created: " << path << std::endl;
            } else {
                std::cerr << "Failed to create directory: " << path << std::endl;
            }
        }
    }

    bool check_model_exists(std::string file_name){
        std::filesystem::path model_path(file_name);
        return std::filesystem::exists(model_path);
    }

    void print_model_summary(const torch::nn::Module& model) {
        std::cout << "Model Summary:\n";
        std::cout << "---------------------------------\n";

        for (const auto& pair : model.named_parameters()) {
            const std::string& name = pair.key();
            const torch::Tensor& param = pair.value();
            std::cout << name << ": " << param.sizes() << "\n";
        }

        std::cout << "---------------------------------\n";
    }

};
