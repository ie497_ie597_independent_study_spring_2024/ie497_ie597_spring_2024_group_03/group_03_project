// Refer to the header files for the explanation of the functions in detail
// In VSCode, hovering your mouse above the function renders the explanation of from the .h file as a pop up
#ifdef _WIN32
    #include "stdafx.h"
#endif

#include "DQNStrategy.h"
#include "experience.h"

using namespace RCM::StrategyStudio;
using namespace RCM::StrategyStudio::MarketModels;
using namespace RCM::StrategyStudio::Utilities;

using namespace std;

// Constructor to initialize member variables of the class to their initial values.
DQNStrategy::DQNStrategy(StrategyID strategyID,
                    const string& strategyName,
                    const string& groupName):
    Strategy(strategyID, strategyName, groupName),
    epsilon(1.0),
    volume_clip(20.0),
    epsilon_decay(0.00001),
    epsilon_end(0.01),
    window_size(10),
    normalise_factor(1.0),
    normalise_window_size(10),
    vol_window_size(20),
    n_parameters(4),
    memory_size(100),
    batch_size(32),
    bar_interval(1),
    target_update_frequency(1000),
    gamma(0.99),
    new_model(false),
    lot_size(1),
    eval(false),
    strategy_active_bool(true){
    }
// Destructor for class
DQNStrategy::~DQNStrategy(){}

void DQNStrategy::DefineStrategyParams(){
    params().CreateParam(CreateStrategyParamArgs("epsilon", STRATEGY_PARAM_TYPE_STARTUP, VALUE_TYPE_DOUBLE, epsilon));
    params().CreateParam(CreateStrategyParamArgs("epsilon_decay", STRATEGY_PARAM_TYPE_STARTUP, VALUE_TYPE_DOUBLE, epsilon_decay));
    params().CreateParam(CreateStrategyParamArgs("epsilon_end", STRATEGY_PARAM_TYPE_STARTUP, VALUE_TYPE_DOUBLE, epsilon_end));
    params().CreateParam(CreateStrategyParamArgs("volume_clip", STRATEGY_PARAM_TYPE_STARTUP, VALUE_TYPE_DOUBLE, volume_clip));
    params().CreateParam(CreateStrategyParamArgs("window_size", STRATEGY_PARAM_TYPE_STARTUP, VALUE_TYPE_INT, window_size)); 
    params().CreateParam(CreateStrategyParamArgs("normalise_factor", STRATEGY_PARAM_TYPE_STARTUP, VALUE_TYPE_DOUBLE, normalise_factor));    
    params().CreateParam(CreateStrategyParamArgs("normalise_window_size", STRATEGY_PARAM_TYPE_STARTUP, VALUE_TYPE_INT, normalise_window_size));
    params().CreateParam(CreateStrategyParamArgs("vol_window_size", STRATEGY_PARAM_TYPE_STARTUP, VALUE_TYPE_INT, vol_window_size));
    params().CreateParam(CreateStrategyParamArgs("n_parameters", STRATEGY_PARAM_TYPE_STARTUP, VALUE_TYPE_INT, n_parameters));
    params().CreateParam(CreateStrategyParamArgs("memory_size", STRATEGY_PARAM_TYPE_STARTUP, VALUE_TYPE_INT, memory_size));
    params().CreateParam(CreateStrategyParamArgs("batch_size", STRATEGY_PARAM_TYPE_STARTUP, VALUE_TYPE_INT, batch_size));
    params().CreateParam(CreateStrategyParamArgs("bar_interval", STRATEGY_PARAM_TYPE_STARTUP, VALUE_TYPE_INT, bar_interval));
    params().CreateParam(CreateStrategyParamArgs("target_update_frequency", STRATEGY_PARAM_TYPE_STARTUP, VALUE_TYPE_INT, target_update_frequency));
    params().CreateParam(CreateStrategyParamArgs("gamma", STRATEGY_PARAM_TYPE_STARTUP, VALUE_TYPE_DOUBLE, gamma));
    params().CreateParam(CreateStrategyParamArgs("new_model", STRATEGY_PARAM_TYPE_STARTUP, VALUE_TYPE_BOOL, new_model));
    params().CreateParam(CreateStrategyParamArgs("lot_size", STRATEGY_PARAM_TYPE_STARTUP, VALUE_TYPE_INT, lot_size));
    params().CreateParam(CreateStrategyParamArgs("eval", STRATEGY_PARAM_TYPE_STARTUP, VALUE_TYPE_BOOL, eval));
}

void DQNStrategy::DefineStrategyCommands(){}

// By default, SS will register to trades/quotes/depth data for the instruments you have requested via command_line.
void DQNStrategy::RegisterForStrategyEvents(StrategyEventRegister* eventRegister, DateType currDate){
    int bar_interval;
    if (!params().GetParam("bar_interval")->Get(&bar_interval)){
        cout << "Error getting bar interval parameter" << endl;
        return;
    }
    for (SymbolSetConstIter it = symbols_begin(); it != symbols_end(); ++it) {
        eventRegister->RegisterForBars(*it, BAR_TYPE_TIME, bar_interval);
    }
    if (!params().GetParam("n_parameters")->Get(&n_parameters) or !params().GetParam("window_size")->Get(&window_size)){
        cout << "Error getting n_parameters or window_size parameter" << endl;
        return;
    }
    state_size = n_parameters*window_size;
    n_actions = 3;
    eventRegister->RegisterForSingleScheduledEvent("day_end",USEquityCloseUTCTime(currDate)-boost::posix_time::seconds(10));
    eventRegister->RegisterForRecurringScheduledEvents("hourly_ping", USEquityOpenUTCTime(currDate), USEquityCloseUTCTime(currDate), boost::posix_time::hours(1));
}

void DQNStrategy::OnTrade(const TradeDataEventMsg& msg) {}

void DQNStrategy::OnScheduledEvent(const ScheduledEventMsg& msg) {
    if (msg.scheduled_event_name() == "hourly_ping"){
        for (InstrumentSetConstIter it = instrument_begin(); it != instrument_end(); ++it) {
        const Instrument* instrument = it->second;
        RecordAccountStats(instrument);
        }
    }
    if (msg.scheduled_event_name() == "day_end"){
        cout << "Day end" << endl;
        strategy_active_bool = false;
        for (InstrumentSetConstIter it = instrument_begin(); it != instrument_end(); ++it) {
            const Instrument* instrument = it->second;
            done_flag[instrument]=true;
            StrategyLoop(instrument);
        }
    }
}

void DQNStrategy::OnOrderUpdate(const OrderUpdateEventMsg& msg) {}

void DQNStrategy::OnBar(const BarEventMsg& msg){
    if (strategy_active_bool){
        const Instrument* instrument = &msg.instrument();
        StrategyLoop(instrument);
    }
}

void DQNStrategy::StrategyLoop(const Instrument* instrument){
    if (eval){
        EvaluateLoop(instrument);
    }
    else{
        TrainingLoop(instrument);
    }
}

void DQNStrategy::OnQuote(const QuoteEventMsg& msg){
}

void DQNStrategy::TrainingLoop(const Instrument* instrument){
    // checks if all state tracking vectors are initialised
    CheckAndInitialiseVectors(instrument);
    // updates the data for the instrument
    UpdateData(instrument);
    // checks if all data is initialised
    if (!(DataFullyInitialised(instrument))){
        return;
    }
    // generates the state for the instrument
    Generate1DState(instrument);
    if (!(state_map[instrument].FullyInitialized())){
        return;
    }
    // gets the previous state upon which the action was taken.
    vector<double> current_state = prev_state[instrument];
    //gets the current state as the future state for the experience.
    vector<double> next_state = state_map[instrument].GetState();
    // updates the previous state to the current state
    prev_state[instrument] = next_state;
    // calculates the reward for the action taken previously
    double current_pnl = portfolio().total_pnl();
    double reward = current_pnl - previous_pnl[instrument];
    previous_pnl[instrument] = portfolio().total_pnl();
    // checks if the episode is done
    bool done = done_flag[instrument];
    // adds the experience to the replay buffer
    int half_range = n_actions / 2;
    replay_buffer_map[instrument].addExperience(current_state, previous_action[instrument] + half_range, reward, next_state, done, epsilon_map[instrument]);
    // gets the next action to be taken 
    int action = GetAction(instrument, next_state, n_actions);
    // cout << "Action is " << action << endl;
    // updates the previous action to the current action
    previous_action[instrument] = action;
    // takes the current action
    if (done_flag[instrument]){
        //liquidateAll(instrument);
        cout << "entered liquidation" << endl;
    }
    else{
        takeAction(instrument, action);
    }

    // learning step
    int batch_size;
    if (!params().GetParam("batch_size")->Get(&batch_size)){
        cout << "Error getting batch size parameter" << endl;
        return;
    }
    if (replay_buffer_map[instrument].getSize() > batch_size){
        // cout << "Learning from batch" << endl;
        std::vector<Experience> batch = replay_buffer_map[instrument].getBatch(batch_size);
        // replay_buffer_map[instrument].debug_batch(batch, false);
        dqn_agent_map[instrument].learn(batch);
    }
}

void DQNStrategy::EvaluateLoop(const Instrument* instrument){
    // checks if all state tracking vectors are initialised
    CheckAndInitialiseVectors(instrument);
    // updates the data for the instrument
    UpdateData(instrument);
    // checks if all data is initialised
    if (!(DataFullyInitialised(instrument))){
        return;
    }
    // generates the state for the instrument
    Generate1DState(instrument);
    if (!(state_map[instrument].FullyInitialized())){
        return;
    }

    vector<double> current_state = state_map[instrument].GetState();
    int action = GetAction(instrument, current_state, n_actions);
    // cout << "Action is " << action << endl;
    // updates the previous action to the current action
    previous_action[instrument] = action;
    // takes the current action
    if (done_flag[instrument])
        liquidateAll(instrument);
    else
        takeAction(instrument, action);
}

void DQNStrategy::OnMarketState(const MarketStateEventMsg& msg){
    if (msg.market_state() == MARKET_STATE_CLOSE){
        cout << "Market is closing" << endl;
        for (InstrumentSetConstIter it = instrument_begin(); it != instrument_end(); ++it) {
        const Instrument* instrument = it->second;
        // RecordAccountStats(instrument);
        }
        cout << "strategy reset for next day" << endl;
        strategy_active_bool = true;
        for (InstrumentSetConstIter it = instrument_begin(); it != instrument_end(); ++it) {
            const Instrument* instrument = it->second;
            if (prev_state.find(instrument) != prev_state.end()){
                prev_state[instrument] = vector<double>(state_size, 0.0);
                previous_action[instrument] = 0;
                done_flag[instrument] = false;
                cout << "current epsilon is " << epsilon_map[instrument] << endl;
                if (dqn_agent_map.find(instrument) != dqn_agent_map.end()){
                    dqn_agent_map[instrument].save_agent();
                }
            }
        }
    }
}

void DQNStrategy::OnResetStrategyState(){
}

void DQNStrategy::OnParamChanged(StrategyParam& param) {}
