#include <torch/torch.h>

// Define a new Module.
struct Net : torch::nn::Module {
    // Modify the constructor to accept input and output sizes.
    Net(int input_size, int output_size) {
        // Construct and register Linear submodules with dynamic sizes.
        fc1 = register_module("fc1", torch::nn::Linear(input_size, 64));
        fc2 = register_module("fc2", torch::nn::Linear(64, 32));
        fc3 = register_module("fc3", torch::nn::Linear(32, output_size));

        // Initialize all non-bias weights with Glorot uniform.
        init_weights();
    }

    // Implement the Net's algorithm.
    torch::Tensor forward(torch::Tensor x) {
        x = torch::relu(fc1->forward(x.reshape({x.size(0), -1})));
        x = torch::dropout(x, /*p=*/0.1, /*train=*/is_training());
        x = torch::relu(fc2->forward(x));
        x = torch::dropout(x, /*p=*/0.1, /*train=*/is_training());
        x = fc3->forward(x);  // Assuming this is the output layer
        return x;
    }
    
    // apply glorot initialisation
    void init_weights() {
        auto parameters = named_parameters();
        for (auto& p : parameters) {
            if (p.key().find("bias") != std::string::npos) {
                p.value().data().zero_();
            } else if (p.key().find("weight") != std::string::npos) {
                auto fan_in = p.value().size(1);
                auto fan_out = p.value().size(0);
                double std = std::sqrt(6. / (fan_in + fan_out));
                p.value().data().uniform_(-std, std);
            }
        }
    }

    // Module subcomponents are declared here.
    torch::nn::Linear fc1{nullptr}, fc2{nullptr}, fc3{nullptr};
};