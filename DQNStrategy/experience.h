// Experience.h
#ifndef EXPERIENCE_H
#define EXPERIENCE_H

#include <vector>
#include <tuple>

using Experience = std::tuple<std::vector<double>, int, float, std::vector<double>, int>;

#endif // EXPERIENCE_H