// This is a preprocessor directive that ensures that the header file is included only once in a given compilation unit, to avoid multiple definitions.
#pragma once

// These are include guards that prevent redefinition of class names, macro constants, and typedef names. 
// Include guards help avoiding name conflicts in large software projects.
#ifndef DQNSTRATEGY_H
#define DQNSTRATEGY_H

// This is a conditional preprocessor directive that defines a macro _STRATEGY_EXPORTS as __declspec(dllexport) on Windows platform, and empty on other platforms.
// This macro is used to export the HelloworldStrategy class to the dynamic link library (DLL) that is loaded by the trading engine.
#ifdef _WIN32
    #define _STRATEGY_EXPORTS __declspec(dllexport)
#else
    #ifndef _STRATEGY_EXPORTS
    #define _STRATEGY_EXPORTS
    #endif
#endif

/**
 * Below are header files that are used by the HelloworldStrategy class. We just tell the compiler to look for these files.
 * You will not have Strategy.h & Instrument.h in your directory. These are part of the SDK.
 * Strategy.h is the main header file for the strategy development kit and provides access to the core functionality of the trading engine.
 * Instrument.h is a header file for instrument specific data. 
 * The remaining headers provide various utility functions.
**/
#include <Strategy.h>
#include <MarketModels/Instrument.h>
#include <MarketModels/IAggrOrderBook.h>
#include <Analytics/ScalarRollingWindow.h>
#include <Analytics/InhomogeneousOperators.h>
#include <Analytics/IncrementalEstimation.h>
#include <Utilities/ParseConfig.h>
#include <IPositionRecord.h>
#include <Order.h>
#include <BarDataTypes.h>
#include <IOrderTracker.h>

#include <string>
#include <unordered_map>
#include <iostream>
#include <algorithm> 
#include <cmath>
#include <memory>
#include <vector>
#include <deque>
// Class declaration
#include <sqlite_modern_cpp.h>
#include <torch/torch.h>
#include "ExperienceReplayDatabase.h"
#include "experience.h"
#include "Agent.h"

// Import namespace RCM::StrategyStudio to avoid explicit namespace qualification when using names from this namespace
using namespace RCM::StrategyStudio;
using namespace RCM::StrategyStudio::Utilities;
using namespace RCM::StrategyStudio::MarketModels;
using namespace std;

class SpreadZScore
{
public:
    SpreadZScore() : window(0), normalise_factor(0.0) {} 
    SpreadZScore(int window_size, double normalise_factor) : window(window_size), normalise_factor(normalise_factor) {}

    void Reset()
    {
        window.clear();
    }

    void Update(double val)
    {
        window.push_back(val);
    }

    double GetSpread(){
        return clipAndScale(window.ZScore());
    }

    double clipAndScale(double value) {
        return max(-normalise_factor, min(value, normalise_factor))/ normalise_factor;
    }

    bool FullyInitialized() { return window.full(); }

private:
    Analytics::ScalarRollingWindow<double> window;
    double normalise_factor;
};

class VolatilityZScore
{
public:
    VolatilityZScore() : price_window(0),vol_window(0), normalise_factor(0.0) {} 
    VolatilityZScore(int vol_window_size, int normalise_window_size, double normalise_factor) : price_window(vol_window_size),vol_window(normalise_window_size),normalise_factor(normalise_factor) {}
    void Reset()
    {
        price_window.clear();
        vol_window.clear();
    }

    void UpdateVol(double val)
    {
        price_window.push_back(val);
        if (PriceFullyInitialized()){
            vol_window.push_back(price_window.StdDev());
        }
    }

    double GetVol(){
        return clipAndScale(vol_window.ZScore());
    }

    double clipAndScale(double value) {
        return max(-normalise_factor, min(value, normalise_factor))/ normalise_factor;
    }

    bool PriceFullyInitialized() { return price_window.full(); }

private:
    Analytics::ScalarRollingWindow<double> price_window;
    Analytics::ScalarRollingWindow<double> vol_window;
    double normalise_factor;
};

class StateMap2D
{
public:
    StateMap2D() : window_size(0) {}
    StateMap2D(int window_size) : window_size(window_size) {}
    
    void Reset()
    {
        state_map.clear();
    }

    std::vector<torch::Tensor> UpdateState(torch::Tensor val)
    {
        // Add the new value to the window
        if (state_map.size() == window_size) {
            // If the window is full, remove the oldest element first
            state_map.pop_front();
        }
        state_map.push_back(val);

        // Return a vector containing all tensors in the current window
        return std::vector<torch::Tensor>(state_map.begin(), state_map.end());
    }

    bool FullyInitialized() const
    {
        return state_map.size() == window_size;
    }

private:
    std::deque<torch::Tensor> state_map;
    int window_size;
};

class StateMap1D
{
public:
    StateMap1D() : state_map(0) {}
    StateMap1D(int window_size, int n_parameters) : state_map(window_size * n_parameters) {}
    void Reset()
    {
        state_map.clear();
    }

    void UpdateState(vector<double> val_vector)
    {
        for (auto const& x : val_vector){
            state_map.push_back(x);
        }
    }

    vector<double> GetState(){
        vector<double> state_vector;
        for (auto const& x : state_map){
            state_vector.push_back(x);
        }
        return state_vector;
    }

    bool FullyInitialized() { return state_map.full(); }

private:
    Analytics::ScalarRollingWindow<double> state_map;
};

class ReturnsZScore
{
public:
    ReturnsZScore() : window(0), last_val(0), normalise_factor(0.0) {}
    ReturnsZScore(int window_size, double normalise_factor) : window(window_size),last_val(0),normalise_factor(normalise_factor) {}
    void Reset()
    {
        window.clear();
    }
    void Update(double val)
    {   
        if (last_val == 0){
            last_val = val;
        }
        window.push_back((val-last_val)/last_val);
    }

    double GetReturn(){
        return clipAndScale(window.ZScore());
    }

    double clipAndScale(double value) {
        return max(-normalise_factor, min(value, normalise_factor))/ normalise_factor;
    }

    bool FullyInitialized() { return window.full(); }

private:
    Analytics::ScalarRollingWindow<double> window;
    double last_val;
    double normalise_factor;
};

class DQNStrategy : public Strategy {
public:
    // defining the spread map
    typedef boost::unordered_map<const Instrument*, SpreadZScore> SpreadMap; 
    typedef SpreadMap::iterator SpreadMapIterator;

    // defining the volatility map
    typedef boost::unordered_map<const Instrument*, VolatilityZScore> VolatilityMap;
    typedef VolatilityMap::iterator VolatilityMapIterator;

    typedef boost::unordered_map<const Instrument*, ReturnsZScore> ReturnsMap;
    typedef ReturnsMap::iterator ReturnsMapIterator;

    typedef boost::unordered_map<const Instrument*, StateMap1D> StateMapObject;
    typedef StateMapObject::iterator StateMapIterator;

    typedef boost::unordered_map<const Instrument*, ExperienceReplayDatabase> ReplayBufferMap;
    typedef ReplayBufferMap::iterator ReplayBufferMapIterator;

    typedef boost::unordered_map<const Instrument*, DQNAgent > DQNAgentMap;
    typedef DQNAgentMap::iterator DQNAgentMapIterator;
    
public:
    // Constructor & Destructor functions for this class
    DQNStrategy(StrategyID strategyID,
        const std::string& strategyName,
        const std::string& groupName);
    ~DQNStrategy();

public:

    // Below are event handling functions the user can override on the cpp file to create their own strategies.
    // Polymorphic behavior is achieved in C++ by using virtual functions, which allows the same function to behave differently depending on the type of object it is called on.

    /**
    * Called whenever a bar event occurs for an instrument that the strategy is subscribed to.
    * A bar event is a notification that a new bar has been formed in the price data of the instrument, where a bar represents a fixed period of time (e.g., 1 minute, 5 minutes, 1 hour) and contains information such as the opening and closing price, highest and lowest price, volume for that period.
    * The msg parameter of the OnBar function is an object of type BarEventMsg that contains information about the bar event that occurred.
    **/
    virtual void OnBar(const BarEventMsg& msg);

    /**
    * Called whenever a trade event occurs for an instrument that the strategy is subscribed to.
    * A trade event refers to a specific occurrence related to a trade of a financial instrument, such as a stock or a commodity like the execution of a buy or sell order
    * The msg parameter is an object of type TradeDataEventMsg that contains information about the trade event that occurred
    */
    virtual void OnTrade(const TradeDataEventMsg& msg);

    /**
     * This event triggers whenever a new quote for a market center arrives from a consolidate or direct quote feed,
     * or when the market center's best price from a depth of book feed changes.
     *
     * User can check if quote is from consolidated or direct, or derived from a depth feed. This will not fire if
     * the data source only provides quotes that affect the official NBBO, as this is not enough information to accurately
     * mantain the state of each market center's quote.
     */ 
    virtual void OnQuote(const QuoteEventMsg& msg);

    /**
     * This event triggers whenever a order book message arrives. This will be the first thing that
     * triggers if an order book entry impacts the exchange's DirectQuote or Strategy Studio's TopQuote calculation.
     */ 
    virtual void OnDepth(const MarketDepthEventMsg& msg){}

    /**
     * Called when a scheduled event occurs during the backtesting process.
     * Examples of actions include making trading decisions, adjusting parameters or indicators, updating strategy state, or triggering specific actions at predefined intervals and time-dependent trading strategies.
    */
    virtual void OnScheduledEvent(const ScheduledEventMsg& msg);

    // Called whenever there is an update to one of the orders placed by the strategy
    void OnOrderUpdate(const OrderUpdateEventMsg& msg);
 
    void OnResetStrategyState();

    void OnParamChanged(StrategyParam& param);

private:

    virtual void RegisterForStrategyEvents(StrategyEventRegister* eventRegister, DateType currDate);

    virtual void DefineStrategyParams();

    virtual void DefineStrategyCommands();

    void OnMarketState(const MarketStateEventMsg& msg);

    void TrainingLoop(const Instrument* instrument);

    void EvaluateLoop(const Instrument* instrument);

    void StrategyLoop(const Instrument* instrument);
    
    void CheckAndInitialiseVectors(const Instrument* instrument){
        double volume_clip;
        double epsilon_decay;
        int window_size;
        double normalise_factor;
        int normalise_window_size;
        int vol_window_size;
        int n_parameters;
        int memory_size;
        double gamma;
        double epsilon;
        int target_update_frequency;
        bool new_model;

        if (!(params().GetParam("volume_clip")->Get(&volume_clip)) or 
        !(params().GetParam("epsilon_decay")->Get(&epsilon_decay)) or 
        !(params().GetParam("window_size")->Get(&window_size)) or 
        !(params().GetParam("normalise_factor")->Get(&normalise_factor)) or 
        !(params().GetParam("normalise_window_size")->Get(&normalise_window_size)) or 
        !(params().GetParam("vol_window_size")->Get(&vol_window_size)) or 
        !(params().GetParam("n_parameters")->Get(&n_parameters)) or 
        !(params().GetParam("memory_size")->Get(&memory_size)) or 
        !(params().GetParam("gamma")->Get(&gamma)) or 
        !(params().GetParam("epsilon")->Get(&epsilon)) or 
        !(params().GetParam("target_update_frequency")->Get(&target_update_frequency)) or 
        !(params().GetParam("new_model")->Get(&new_model))){
            cout << "Error getting parameters" << endl;
            return;
        }
        int state_size = n_parameters*window_size;
        if (spread_map.find(instrument) == spread_map.end()){
            spread_map[instrument] = SpreadZScore(window_size,normalise_factor);
            cout << "created new spread map" << endl;
        }
        if (volatility_map.find(instrument) == volatility_map.end()){
            volatility_map[instrument] = VolatilityZScore(vol_window_size,normalise_window_size,normalise_factor);
            cout << "created new volatility map" << endl;
        }
        if (returns_map.find(instrument) == returns_map.end()){
            returns_map[instrument] = ReturnsZScore(window_size,normalise_factor);
            cout << "created new returns map" << endl;
        }
        if (state_map.find(instrument) == state_map.end()){
            state_map[instrument] = StateMap1D(window_size,n_parameters);
            cout << "created new state map" << endl;
        }
        if (replay_buffer_map.find(instrument) == replay_buffer_map.end()){
            cout << "new_model is " << new_model << endl;
            string instrument_db = "/groupstorage/group01/DQNStrategy/dbs/" + instrument->symbol() + "_memory.db";
            replay_buffer_map[instrument] = ExperienceReplayDatabase(instrument_db,memory_size,new_model);
            cout << "created new replay buffer map" << endl;
        }
        if (dqn_agent_map.find(instrument) == dqn_agent_map.end()){
            dqn_agent_map[instrument] = DQNAgent(state_size, n_actions, gamma, target_update_frequency,instrument->symbol(),new_model);
            dqn_agent_map[instrument].print_agent_model_summary();
            cout << "created new dqn agent map" << endl;
        }
        if (prev_state.find(instrument) == prev_state.end()){
            prev_state[instrument] = vector<double>(state_size,0.0);
            cout << "created new prev state map" << endl;
        }
        if (previous_pnl.find(instrument) == previous_pnl.end()){
            previous_pnl[instrument] = 0.0;
            cout << "created new previous pnl map" << endl;
        }
        if (previous_action.find(instrument) == previous_action.end()){
            previous_action[instrument] = 0;
            cout << "created new previous action map" << endl;
        }
        if (!(new_model))
            epsilon = replay_buffer_map[instrument].getEpisodeInfo().epsilon;
        if (epsilon_map.find(instrument) == epsilon_map.end()){
            epsilon_map[instrument] = epsilon;
            cout << "created new epsilon map" << endl;
        }
        if (done_flag.find(instrument) == done_flag.end()){
            done_flag[instrument] = false;
        }
    }

    void UpdateData(const Instrument* instrument){
        const IAggrOrderBook &orderBook = instrument->aggregate_order_book();
        double mid_price = CalculateMidPrice(orderBook);
        double spread = CalculateSpread(orderBook);
        spread_map[instrument].Update(spread);
        volatility_map[instrument].UpdateVol(mid_price);
        returns_map[instrument].Update(mid_price);
    }

    bool DataFullyInitialised(const Instrument* instrument){
        return spread_map[instrument].FullyInitialized() && volatility_map[instrument].PriceFullyInitialized() && returns_map[instrument].FullyInitialized();
    }

    double CalculateImbalance(const IAggrOrderBook& orderBook){
        double ask_size = static_cast<double>(orderBook.TotalAskSize());
        double bid_size = static_cast<double>(orderBook.TotalBidSize());
        return (ask_size - bid_size) / (ask_size + bid_size);
    }

    double CalculateMidPrice(const IAggrOrderBook& orderBook) {
        double mid_price = (orderBook.BestAskLevel()->price() + orderBook.BestBidLevel()->price()) / 2;
        return mid_price;
    }

    double CalculateSpread(const IAggrOrderBook& orderBook){
        double spread = orderBook.BestAskLevel()->price() - orderBook.BestBidLevel()->price();
        return spread;
    }

    void Generate1DState(const Instrument* instrument){
        const IAggrOrderBook &orderBook = instrument->aggregate_order_book();
        double imbalance = CalculateImbalance(orderBook);
        double spread_state = spread_map[instrument].GetSpread();
        double volatility_state = volatility_map[instrument].GetVol();
        double returns_state = returns_map[instrument].GetReturn();

        vector<double> state_vector;

        state_vector.push_back(imbalance);
        state_vector.push_back(spread_state);
        state_vector.push_back(volatility_state);
        state_vector.push_back(returns_state);
        state_map[instrument].UpdateState(state_vector);
    }

    int GetAction(const Instrument* instrument, vector<double> next_state, int n_actions = 3){
        double epsilon_decay;
        if (!(params().GetParam("epsilon_decay")->Get(&epsilon_decay))){
            return 0;
        }
        // updating epsilon
        double epsilon = epsilon_map[instrument];
        if (epsilon != 0.0){ // we are evaluating the model.
            epsilon = max(0.1, epsilon - epsilon_decay);
            epsilon_map[instrument] = epsilon;
        }
        // action space
        vector<int> action_vec = createActionVec(n_actions);
        double randomValue = static_cast<double>(rand()) / RAND_MAX;
        if (randomValue < epsilon and !(eval)){
            std::size_t index = std::rand() % action_vec.size();
            return action_vec[index];
        }
        else{
            int index = dqn_agent_map[instrument].get_action(next_state);
            return action_vec[index];
        }
    }

    vector<int> createActionVec(int n_actions) {
        // Check if n_actions is odd
        if (n_actions % 2 == 0) {
            throw std::invalid_argument("The number of actions must be odd.");
        }

        std::vector<int> action_vec;
        int half_range = n_actions / 2;

        // Fill the vector with symmetric positive and negative values around zero
        for (int i = -half_range; i <= half_range; ++i) {
            action_vec.push_back(i);
        }

        return action_vec;
    }

    void takeAction(const Instrument* instrument, int action){
        double ask_price = instrument->aggregate_order_book().BestAskLevel()->price();
        double bid_price = instrument->aggregate_order_book().BestBidLevel()->price();
        int ask_volume = instrument->aggregate_order_book().BestAskLevel()->size();
        int bid_volume = instrument->aggregate_order_book().BestBidLevel()->size();
        if (!(params().GetParam("lot_size")->Get(&lot_size))){
            return;
        }
        if (action == 1){
            // cout << "Buying the ask" << endl;
            // cout << "Buy price is " << ask_price << endl;
            // cout << "Buy volume is " << ask_volume << endl;
            OrderParams params(*instrument,
                        lot_size,
                        ask_price,
                        MARKET_CENTER_ID_NASDAQ,
                        ORDER_SIDE_BUY,
                        ORDER_TIF_IOC,
                        ORDER_TYPE_LIMIT);
            trade_actions()->SendNewOrder(params);
        }
        else if (action == -1){
            // cout << "Selling the bid" << endl;
            // cout << "Sell price is " << bid_price << endl;
            // cout << "Sell volume is " << bid_volume << endl;
            OrderParams params(*instrument,
                        lot_size,
                        bid_price,
                        MARKET_CENTER_ID_NASDAQ,
                        ORDER_SIDE_SELL_SHORT,
                        ORDER_TIF_IOC,
                        ORDER_TYPE_LIMIT);
            trade_actions()->SendNewOrder(params);
        }
        else{
            return;
        }
    }

    void liquidateAll(const Instrument* instrument){
        int inventory = portfolio().position(instrument);
        double price = (inventory > 0) ? instrument->top_quote().bid() - 10 : instrument->top_quote().ask() + 10;
        OrderSide action = (inventory > 0) ? ORDER_SIDE_SELL : ORDER_SIDE_BUY;

        OrderParams params_(*instrument,
                        abs(inventory),
                        price,
                        MARKET_CENTER_ID_NASDAQ,
                        action,
                        ORDER_TIF_DAY,
                        ORDER_TYPE_LIMIT);
        trade_actions()->SendNewOrder(params_);
    }

    void RecordAccountStats(const Instrument* instrument){
        cout << "instrument is " << instrument->symbol() << endl;
        cout << "Total PNL " << portfolio().total_pnl(instrument) << endl;
        cout << "Unrealised PNL " << portfolio().unrealized_pnl(instrument) << endl;
        cout << "Realised PNL " << portfolio().realized_pnl(instrument) << endl;
    }

private:
    // Used to store the state and data of the strategy.
    double epsilon;
    double volume_clip;
    double epsilon_decay;
    int window_size;
    double normalise_factor;
    int normalise_window_size;
    int vol_window_size;
    int n_parameters;
    int memory_size;
    bool new_model;
    int batch_size;
    double gamma;
    int bar_interval;
    int target_update_frequency;
    double epsilon_end;
    int lot_size;
    bool eval;
    bool strategy_active_bool;

    boost::unordered_map<const Instrument*, vector<double> > prev_state;
    boost::unordered_map<const Instrument*, double > previous_pnl;
    boost::unordered_map<const Instrument*, int > previous_action;
    boost::unordered_map<const Instrument*, double > epsilon_map;
    boost::unordered_map<const Instrument*, bool > done_flag;

    int n_actions;
    int state_size;
    SpreadMap spread_map;
    VolatilityMap volatility_map;
    ReturnsMap returns_map;
    StateMapObject state_map;
    ReplayBufferMap replay_buffer_map;
    DQNAgentMap dqn_agent_map;
};

// extern "C" is used to tell the compiler that these functions have C-style linkage instead of C++-style linkage, which means the function names will not be mangled.
// Except the strategy name, you don't need to change anything in this section.
extern "C" {

    _STRATEGY_EXPORTS const char* GetType() {
        return "DQNStrategy";
    }

    _STRATEGY_EXPORTS IStrategy* CreateStrategy(const char* strategyType,
                                   unsigned strategyID,
                                   const char* strategyName,
                                   const char* groupName) {
        if (strcmp(strategyType, GetType()) == 0) {
            return *(new DQNStrategy(strategyID, strategyName, groupName));
        } else {
            return NULL;
        }
    }

    _STRATEGY_EXPORTS const char* GetAuthor() {
        return "dlariviere";
    }

    _STRATEGY_EXPORTS const char* GetAuthorGroup() {
        return "UIUC";
    }

    _STRATEGY_EXPORTS const char* GetReleaseVersion() {
        return Strategy::release_version();
    }
}
// The #endif statement marks the end of the include guard to prevent the header file from being included multiple times.
#endif