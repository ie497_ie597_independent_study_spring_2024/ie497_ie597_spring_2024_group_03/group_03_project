# Development and Implementation of a Reinforcement Learning Based Agents in High Frequency Trading Using Strategy Studio for Backtesting

Authored by: Samanvay Malapally Sudhakara [MSFE '24, UIUC] [linkedin](https://www.linkedin.com/in/samanvay-malapally-sudhakara-148836212/)

This project report details the design, implementation, and backtesting of a Deep Deterministic Policy Gradient (DDPG) Market Making Agent and a Deep Q Network (DQN) Market Taking Agent, to explore innovative approaches to algorithmic trading. The Market Making agent is designed to operate in financial markets, aiming to earn profits from bid-ask spreads while managing inventory levels and mitigating risks while the Market Taking Agent is aimed at generating alpha by decisively buying and selling on the market. Strategy Studio was employed as the primary software for backtesting the agent's performance in simulated market environments. This project encapsulates a multi-disciplinary approach, integrating deep learning, reinforcement learning (RL), and quantitative finance methodologies.

## Table of Contents

- [Introduction](#introduction)
  - [Market Making](#market-making)
  - [Using Reinforcement Learning in Market Making](#using-reinforcement-learning-in-market-making)
  - [Q-Learning](#q-learning)
  - [Deep Q Networks (DQN)](#Deep-Q-Networks-(DQN))
  - [Deep Deterministic Policy Gradients (DDPG)](#Deep-Deterministic-Policy-Gradients-(DDPG))
  - [Project Objective](#project-objective)
- [Methodology](#methodology)
  - [Environment](#environment)
  - [Experience Replay Database for DQN and DDPG Models](#experience-replay-database-for-dqn-and-ddpg-models)
  - [DDPG Market Making Agent Design](#ddpg-market-making-agent-design)
  - [DQN Market Making Agent Design](#dqn-market-making-agent-design)
- [Results](#results)


## Introduction

### Market Making

Market making is a crucial function in financial markets, serving as the backbone for liquidity and price discovery. At its core, market making involves a participant—commonly a firm or an individual—offering to buy and sell a particular asset at all times, thus providing liquidity to other market participants. This activity is essential in facilitating smooth and efficient market operations, enabling participants to execute trades with minimal delay and at predictable prices.

#### The Role of a Market Maker

A market maker quotes two prices: the **bid** price, at which they are willing to buy an asset, and the **ask** (or offer) price, at which they are willing to sell. The difference between these two prices is known as the **spread**, which represents the market maker's potential profit from their operations, compensating for the risk of holding inventory and the service of providing liquidity.

#### Inventory and Risk Management

Market makers manage an inventory of the securities they trade, balancing the need to provide liquidity with the risk of holding large positions that might move against them. Sophisticated strategies, including **stochastic calculus**, **algorithmic trading**, and **quantitative finance** principles, are employed to dynamically adjust quotes based on the current market condition, inventory levels, and volatility. This balancing act is essential for maintaining profitability while ensuring market stability.

#### Profitability and Challenges

The profitability of market making depends on successfully managing the spread between the buy and sell prices and effectively managing inventory risk. Market makers face several challenges, including competition from other market participants, market impact costs, and adverse selection risk, where more informed traders might exploit the market maker's quotes. Advanced **AI/ML techniques**, **Generative AI**, and **Deep Neural Networks** are increasingly utilized to analyze market data, predict price movements, and optimize trading strategies, enhancing the market maker's ability to remain competitive and profitable.

#### Contribution to Market Efficiency

By providing continuous buy and sell quotes, market makers facilitate tighter spreads, greater liquidity, and improved price discovery, contributing to the overall efficiency and stability of financial markets. They play a vital role in enabling both retail and institutional investors to execute trades promptly and at fair prices, thus supporting the smooth operation of the global financial system.

In summary, market making is a sophisticated and dynamic area of finance, requiring a deep understanding of market mechanisms, risk management, and advanced technological tools. The ability to adapt to changing market conditions, manage inventory risk, and employ cutting-edge technological solutions is essential for the success and sustainability of market making operations.

### Using Reinforcement Learning in Algorithmic trading

Implementing reinforcement learning (RL) in Algorithmic Trading involves training an agent to dynamically adapt to changing market conditions to optimize the given trading strategies. Here is a structured approach to using RL in this context:

1. **Define the Environment:**
   - Represent the market environment with relevant features, such as price movements, trading volumes, and order book data.
   - Model the state space to capture all necessary market conditions that influence trading decisions.

2. **Design the Agent:**
   - Create an agent that can execute trading actions like setting bid-ask spreads, placing and canceling orders, and adjusting inventory.
   - Define the agent's actions and strategies within the environment.

3. **Establish a Reward System:**
   - Develop a reward function that reflects market-making objectives like maximizing profit, minimizing risk, and improving liquidity.
   - The reward should penalize actions that lead to inventory imbalances or significant market impacts.

4. **Select an RL Algorithm:**
   - Choose an appropriate RL algorithm, such as DQN or DDPG, depending on whether the action space is discrete or continuous.
   - The algorithm should handle the high-dimensionality of market data and the complexity of trading decisions.

5. **Train the Agent:**
   - Collect historical market data to simulate market conditions for training.
   - Use experience replay and target networks to improve training stability and efficiency.
   - Evaluate the agent's performance using backtesting and iteratively refine the model.

6. **Deploy and Monitor:**
   - Test the trained agent in a simulated live trading environment to validate its strategies.
   - Deploy the agent in live trading with close monitoring to ensure it adheres to risk management policies.
   - Continuously monitor and adjust the agent's strategy to adapt to evolving market conditions.

This structured approach allows RL agents to learn optimal market-making strategies, balancing profitability and risk management in real-time trading environments.

![Reinforcement Learning in Action](images/RL_Animation.gif)

#### Key Applications

- **Price Prediction:** RL agents anticipate price changes to determine the best bid-ask spread.
- **Inventory Management:** They control inventory levels to reduce exposure to market risks while ensuring liquidity.
- **Order Execution:** They adapt order placement strategies to minimize market impact and maximize returns.
- **Dynamic Pricing:** RL can adjust bid-ask spreads dynamically based on market conditions to stay competitive.

### Q-Learning

Q-learning is a type of reinforcement learning algorithm that focuses on learning the value of actions (called Q-values) in a given state to determine the optimal policy for an agent. The aim is to maximize the total reward over time by learning which action to take in each state to achieve the highest cumulative reward. Here's a breakdown of how Q-learning works:

#### Core Concepts

- **State (s):** A representation of the environment's current situation.
- **Action (a):** A possible move the agent can make.
- **Reward (r):** Feedback received after performing an action, which can be positive or negative.
- **Q-value (Q(s, a)):** The expected reward of taking action `a` in state `s` and following the optimal policy thereafter.

#### Key Components

- **Q-Table:** A matrix where rows represent states and columns represent actions. The value at `Q(s, a)` indicates the expected reward of taking action `a` in state `s`.
- **Learning Rate (α):** Determines how much newly acquired information overrides old information.
- **Discount Factor (γ):** Measures the importance of future rewards compared to immediate rewards.

![Q-Learning Process](images/Qlearning.gif)

#### Algorithm Steps

1. **Initialize Q-Table:** Start with a Q-table filled with zeros or random values.
2. **Choose Action:** Select an action using an exploration-exploitation strategy like epsilon-greedy (where the agent either exploits the best-known action or explores new actions).
3. **Perform Action and Observe Result:** Take the chosen action, observe the new state and reward.
4. **Update Q-Value:** Update the Q-value for the state-action pair using the Bellman equation:

![formula](images/q_formula.png)

where `s` is the current state, `a` is the action taken, `r` is the reward received, `s'` is the new state, `a'` is the action that maximizes the Q-value in the new state, `α` is the learning rate, and `γ` is the discount factor.

where `s'` is the new state, and `a` is the action that maximizes the Q-value in the new state.

5. **Repeat:** Continue the process until convergence or for a predetermined number of episodes.

#### Strengths and Limitations

- **Strengths:** Q-learning is model-free (doesn't need a model of the environment) and can handle stochastic environments.
- **Limitations:** Struggles with large state-action spaces (high dimensionality) and requires many iterations to learn effectively.

Overall, Q-learning is foundational in reinforcement learning, enabling agents to learn optimal actions in various environments through trial and error.

### Deep Q Networks (DQN)

Deep Q-Networks (DQN) are an evolution in reinforcement learning (RL) that uses deep learning to approximate Q-values for complex, high-dimensional state spaces. The concept was popularized by DeepMind's work on Atari games, where DQNs learned to play directly from pixel inputs.

#### DQN Architecture and Principles

- **Q-Learning:** A model-free RL method where the agent learns the value (Q-value) of taking a particular action in a given state.
- **Neural Network Approximation:** Uses a neural network to approximate the Q-function for high-dimensional state spaces.
- **Architecture:** Typically includes an input layer for state representation (e.g., image pixels), several hidden layers for feature extraction, and an output layer to predict Q-values for all possible actions.

![DQN Architecture](images/DQNtarget_eval.png)

#### DQN Training and Optimization

- **Experience Replay:** Transitions are stored and sampled randomly to reduce correlations in training data.
- **Fixed Q-Targets:** A separate, frozen network generates target Q-values to stabilize training.

#### DQN Applications

- **Gaming:** Effective in video games like Atari, where states and actions are discrete.
- **Robotics and Automation:** Applied to grid-world problems and simple robotic control.

#### DQN Challenges

- **Q-value Overestimation:** Can lead to suboptimal policies.
- **Hyperparameter Sensitivity:** DQN performance is sensitive to various hyperparameters.
- **Data Requirements:** Requires significant data for effective learning.

### Deep Deterministic Policy Gradients (DDPG)

Deep Deterministic Policy Gradient (DDPG) extends RL to continuous action spaces, combining the strengths of policy gradient methods and value-based methods. DDPG builds on the Actor-Critic architecture to handle environments where actions are not discrete.

![DDPG vs DQN](images/DQN_vs_DDPG.png)

#### DDPG Architecture and Principles

- **Actor-Critic:** Uses two networks - an actor that proposes actions and a critic that evaluates them.
- **Deterministic Policy Gradient:** Learns a deterministic policy instead of a stochastic one, which is particularly effective in continuous action spaces.
- **Neural Network Architecture:** The actor and critic networks each have input, hidden, and output layers to approximate the policy and value functions.

#### DDPG Training and Optimization

- **Experience Replay:** Stores transitions and samples them randomly for training to improve data efficiency.
- **Target Networks:** Both the actor and critic networks have corresponding target networks to stabilize training.

#### DDPG Applications

- **Robotics:** Suitable for tasks like robotic manipulation, where actions are continuous.
- **Finance:** Applied to scenarios with continuous decision variables like trading strategies.
- **Autonomous Vehicles:** Helps in tasks involving continuous control of vehicle dynamics.

#### DDPG Challenges

- **Instability in Learning:** Policy and value function updates can lead to unstable training.
- **Sample Efficiency:** Requires a significant amount of exploration to find optimal policies.

In conclusion, while DQN and DDPG cater to different types of environments (discrete vs. continuous action spaces), both have significantly contributed to the development of reinforcement learning methods.

### Project Objective

The primary objective of this project was to design and implement a Market Making Agent capable of learning optimal trading strategies in various market conditions. The Agent would be built and trained by simulating the environment with RCM-Xs Strategy Studio. The model would later be evaluated on the same.

## Methodology

### Environment - Strategy Studio

**Strategy Studio** is an advanced, multi-asset class strategy development platform tailored for algorithmic traders. Designed with efficiency in mind, it enables the rapid implementation, testing, and deployment of complex trading strategies. At its core, Strategy Studio boasts a robust **Strategy Development API** that is data feed and execution provider-agnostic. This flexibility ensures that traders can concentrate fully on refining the logic and effectiveness of their strategies without the constraints of underlying technology stacks.

#### Key Components of Strategy Studio

- **Strategy Development API:** The backbone of the platform, this API facilitates the creation of strategies by abstracting the complexities of data handling and execution. Its provider-agnostic nature allows for seamless integration with various data feeds and execution platforms, enhancing the adaptability of your trading operations.

- **Strategy Servers:** These servers are the operational heart of Strategy Studio, tasked with loading and executing trading strategies efficiently. They ensure that your algorithms perform optimally in real-time trading environments, backed by powerful computational resources.

- **Strategy Manager:** This user interface serves as the command center for both your research and live trading activities. It provides comprehensive tools for performance monitoring and risk management, enabling traders to maintain control over their strategies with precise analytics and real-time oversight.

For more information on Strategy Studio , and how to use it , please refer to the [Strategy Studio Documentation](https://www.rcm-x.com/strategy-studio/), GettingStarted pdf and the [Strategy Studio Guide](StrategyStudioGuide.md) in this repository.

#### As a Reinforcement Learning Environment

The Agent interacts with the strategy studio backtesting environment by first observing the market conditions as a state of features that are most represenative and informative for the agent to make decisions. The agent then takes actions based on the state and the policy it has learned. The agent then receives a reward based on the perfromance of its action it has taken in the backtest and the state it has now ended up in by taking that action. The agent then learns from the reward and updates its policy to maximize the reward.

### Experience Replay

The `ExperienceReplayDatabase` module is an integral part of our project, designed to efficiently manage and store agent experiences for Deep Q-Networks (DQN) and Deep Deterministic Policy Gradient (DDPG) models using an SQLite database. This module is fundamental for implementing the experience replay technique in reinforcement learning, enhancing learning efficiency by reutilizing past experiences to break correlations between consecutive learning samples.

![Experience Replay Database](images/experience_replay.png)

**Database Design:**

- **Schema Details:** The database schema includes fields for storing state, action, reward, next state, and a completion flag (done), along with a timestamp for each entry. This setup captures all necessary data for the training of DQN and DDPG models.
- **Circular Buffer Mechanism:** A maximum capacity (`max_rows`) can be set upon initialization, ensuring that the database does not exceed the specified size. Older entries are automatically removed as new experiences are added, maintaining a current and relevant dataset for training.
- **Data Store and Retrieval:** The database also contains a separate `store` table to save other vital information such as the running count for the buffer and the decayed `epsilon` while training the DQN model and the `noise_std` for the DDPG model.

**Key Functionalities:**

- **Experience Management:** Experiences are added through the `addExperience` method, which handles serialization of state vectors and manages the circular buffer to seamlessly replace old data with new entries. it also updates the various variables in the store table.
- **Batch Retrieval for Training:** The `getBatch` method fetches a random batch of experiences, crucial for the stochastic training processes in DQN and DDPG models. This function is designed to provide a diverse range of samples, crucial for effective training.
- **Data_Store_retrival:** The data store is designed to help seamless carry over of the parameters from one backtest to another and helps in maintaining the state of the agent across multiple backtests. the stored data is retrived using the `getEpisodeInfo` method.

### DQN Market Taking Agent Design

#### 1. DQN Psuedo Code

for every call of OnBar on Strategy Studio the following is executed for the DQN Strategy:-

- Training Loop:-

1. Initialize state vectors for the instrument if not already done.
2. Update market data for the instrument.
3. If data is not fully initialized, exit the function.
4. Generate the current state from the latest market data.
5. If the state is not fully initialized, exit the function.
6. Retrieve the previous state and calculate the reward based on P&L changes.
7. Update the previous state to the current state.
8. Determine if the trading episode is completed.
9. Store the experience (previous state, action, reward, current state, done flag) in the replay buffer.
10. Sample a batch from the replay buffer if it exceeds the batch size and perform a learning update on the DQN agent.
11. Select the next action based on the current state and execute it.
12. Check for episode completion and take necessary actions (like liquidation).

- Evaluation Loop:-

1. Initialize state vectors for the instrument if not already done.
2. Update market data for the instrument.
3. If data is not fully initialized, exit the function.
4. Generate the current state from the latest market data.
5. If the state is not fully initialized, exit the function.
6. Retrieve the optimal action for the current state using the policy derived from the DQN agent.
7. Execute the action.
8. Check for episode completion and take necessary actions (like liquidation).

#### 1. DQN State Representation

The state space included features crucial for decision-making as the model learns the optimal policy based on how representative the state space is. The state space is designed to capture essential market dynamics and conditions that influence trading decisions.

State Features :-

- **Bid Ask Spread** :-
  - Z score normalised value of the bid and ask spread.
  - This helps the model learn whether the spreads are tightening or widening.
  - Updated every time the best bid or ask changes

- **Orderbook Imbalance** :-
  - Order book imbalance measures the relative size difference between buy and sell orders in the order book.
  - Order Book Imbalance = (Volume of Buy Orders - Volume of Sell Orders) / (Volume of Buy Orders + Volume of Sell Orders)
  - Updated every time an action is going to be taken.

- **Volatility of Mid Price** :-
  - n period volatility of the stock price that is z score normalised.
  - this helps the model learn from changes in volatility of the stock price.
  - Updated every second.

- **Returns** :-
  - n period returns of the stock price that is z score normalised.
  - this helps the model learn from the price movements.

The final state space is the above feautures collected over a period of time decided by `window` parameter.

#### 2. DQN Actions

Action space involves just 2 decisions for simplicity , hit the bid or pick the ask price based on what inventory the agent has already accumulated.

- **Training Loop**:
  - The action is selected based on the epsilon-greedy policy, where the agent either exploits the best-known action or explores new actions with a certain probability.
  - The epsilon value decays over time to shift the agent's focus from exploration to exploitation as training progresses.
- **Evaluation Loop**:
  - The action is selected based on the policy derived from the DQN agent, which aims to maximize the expected cumulative reward.

- **Learning Update** (only in training loop):-
  - The DQN agent learns from the experiences stored in the replay buffer by sampling a batch of experiences and updating the Q-values based on the Bellman equation.

#### 3. DQN Reward Function

The reward function would be the temporal difference in profits between subsequent bars as recorded by the environment(Strategy Studio).

#### 4. DQN Psuedo Code

for every call of OnBar on Strategy Studio the following is executed for the DQN Strategy:-

- Training Loop:-

1. Initialize state vectors for the instrument if not already done.
2. Update market data for the instrument.
3. If data is not fully initialized, exit the function.
4. Generate the current state from the latest market data.
5. If the state is not fully initialized, exit the function.
6. Retrieve the previous state and calculate the reward based on P&L changes.
7. Update the previous state to the current state.
8. Determine if the trading episode is completed.
9. Store the experience (previous state, action, reward, current state, done flag) in the replay buffer.
10. Sample a batch from the replay buffer if it exceeds the batch size and perform a learning update on the DQN agent.
11. Select the next action based on the current state and execute it.
12. Check for episode completion and take necessary actions (like liquidation).

- Evaluation Loop:-

1. Initialize state vectors for the instrument if not already done.
2. Update market data for the instrument.
3. If data is not fully initialized, exit the function.
4. Generate the current state from the latest market data.
5. If the state is not fully initialized, exit the function.
6. Retrieve the optimal action for the current state using the policy derived from the DQN agent.
7. Execute the action.
8. Check for episode completion and take necessary actions (like liquidation).

#### 5. DQN Neural Network Architecture

The DQN would consist of two neural networks - the target network and the evaluation network. The target network is used to predict the target Q-values, while the evaluation network is updated during training to minimize the loss between the predicted and target Q-values. Both target and evaluation networks have the same architecture.
Below is the structure of the network, which is implemented using PyTorch:

1. **Input Layer:** This layer takes the state as input and passes it through a fully connected layer (`fc1`). This layer has a dynamic size based on the input size, typically the dimensions of the state. It uses ReLU activation to introduce non-linearity.
2. **Hidden Layer 1:** A fully connected layer (`fc2`) with 256 units. It employs ReLU activation for non-linear processing and is followed by dropout with a dropout probability of 0.1 to prevent overfitting during training.
3. **Hidden Layer 2:** Another fully connected layer (`fc3`) with 64 units. This layer also uses ReLU activation and includes dropout with the same dropout probability to enhance generalization.
4. **Output:** The final layer (`fc3`) is a fully connected layer with a dynamic size based on the output size, which is typically the number of actions. It outputs the action values directly without additional activation, as it is the output layer.

### DDPG Market Making Agent Design

#### 1. DDPG Actions

Continuous action space where the agent can adjust the bid-ask spread while the lot sizes are fixed. The agent then moves the bid and ask prices based on the spread and the current market conditions. action space is a value between -1 and 1 where -1 represents the ask being placed one spread distance above the current ask and 1 represents the ask being placed at the bid price. same for the bid price.

\[
\text{order ask} = \text{ask} - (\text{spread} \times \text{action} / \text{least count}) * \text{least count}
\]

\[
\text{order bid} = \text{bid} + (\text{spread} \times \text{action} / \text{least count}) * \text{least count}
\]

#### 2. DDPG Psuedo Code and State Representation

The pseudo code for the DDPG agent is the same as the DQN agent with the only change being the conversion of the action pair into price values. The state space and reward function is the same as that of the DQN agent.

#### 3. DDPG Neural Network Architecture

The DDPG agent would consist of two neural networks: an actor and a critic. The actor network would output the bid-ask spread adjustment, while the critic network would evaluate the state-action pairs. The neural networks would be trained using the DDPG algorithm, which combines deep learning with policy gradients to optimize the agent's performance.

**Actor:**

1. **Input Layer:** Takes the state as input and passes it through a fully connected layer (`fc1`). This layer dynamically adjusts to the state size using ReLU activation to introduce non-linearity.
2. **Hidden Layer 1:** A fully connected layer (`fc2`) with 256 units and ReLU activation. This layer is followed by dropout with a probability of 0.1 to prevent overfitting during training.
3. **Hidden Layer 2:** Another fully connected layer (`fc3`) with 64 units, also using ReLU activation followed by dropout with a dropout probability of 0.1.
4. **Output:** The final output layer (`fc3`) is a fully connected layer with a size that dynamically adjusts based on the number of actions, using the tanh activation to output action values within a bounded range.

**Critic:**

1. **Input Layer:** Combines the state and action as inputs, passing them through a fully connected layer (`fc1`). This layer adjusts dynamically to the combined size of the state and action dimensions using ReLU activation.
2. **Hidden Layer 1:** A fully connected layer (`fc2`) with 256 units, employing ReLU activation and followed by dropout with a probability of 0.1.
3. **Hidden Layer 2:** Consists of another fully connected layer (`fc3`) with 64 units. It uses ReLU activation and includes dropout with the same dropout probability to enhance generalization.
4. **Output:** The final layer (`fc3`) is a fully connected layer that outputs a single value representing the Q-value, using linear activation to directly express the estimated value of the state-action pair.

### Glorot Weight Initialization

In the development of our model, choosing the appropriate weight initialization method is crucial for effective training and convergence. We have opted for the Glorot (also known as Xavier) initialization due to its well-documented benefits in maintaining a balanced variance in activations and gradients throughout the network. This method is particularly effective for layers that use activation functions such as tanh or sigmoid, which are common in many deep learning architectures.

Glorot initialization sets the weights by drawing them from a distribution with zero mean and a variance that depends on the number of input and output neurons, specifically:

![Glorot Initialization](images/glorot.png)

where `inputs` and `outputs` refer to the number of input and output neurons, respectively. This approach helps prevent the vanishing or exploding gradients problem, facilitating a more stable training process.

The choice of Glorot initialization is underpinned by its ability to help the network learn efficiently. As the gradients are more likely to stay in a reasonable range during training, the network can achieve better performance in fewer epochs, leading to a more efficient learning process.

### Running The Agents in Strategy Studio

#### `Trainer.py` script

The Trainer script is used to train the DQN and DDPG agents in Strategy Studio. The script can used to train the agent , evaluate the strategy given a trained model exists and complete both training and evaluation in a single run.
While Initiating trining we have new_model set to true and for all subsequent runs(episodes) we set it to false.
While evaluating we set the eval_flag to true along with new_model set to false as we want to use the available pretrained model.

```bash
python3 Trainer.py train 100
```

will train the model for 100 episodes.

```bash
python3 Trainer.py eval
```

will evaluate the model.

```bash
python3 Trainer.py all 100
```

will train the model for 100 episodes and then evaluate the model.

Results.

- pending due to minor bugs in the strategy - will be updated soon.
