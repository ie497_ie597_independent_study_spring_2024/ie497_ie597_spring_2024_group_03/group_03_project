// Refer to the header files for the explanation of the functions in detail
// In VSCode, hovering your mouse above the function renders the explanation of from the .h file as a pop up
#ifdef _WIN32
    #include "stdafx.h"
#endif

#include "DDPGStrategy.h"

using namespace RCM::StrategyStudio;
using namespace RCM::StrategyStudio::MarketModels;
using namespace RCM::StrategyStudio::Utilities;

using namespace std;

// Constructor to initialize member variables of the class to their initial values.
DDPGStrategy::DDPGStrategy(StrategyID strategyID,
                    const string& strategyName,
                    const string& groupName):
    Strategy(strategyID, strategyName, groupName),
    tau(1.0),
    gamma(0.99),
    noise_std(0.1),
    volume_clip(20.0),
    window_size(10),
    normalise_factor(1.0),
    normalise_window_size(10),
    vol_window_size(20),
    n_parameters(3),
    lot_size(1),
    memory_size(100),
    least_count(0.01),
    batch_size(32),
    new_model(false),
    eval(false),
    strategy_active_bool(true){
    }
// Destructor for class
DDPGStrategy::~DDPGStrategy(){}

void DDPGStrategy::DefineStrategyParams(){
    params().CreateParam(CreateStrategyParamArgs("tau", STRATEGY_PARAM_TYPE_STARTUP, VALUE_TYPE_DOUBLE, tau));
    params().CreateParam(CreateStrategyParamArgs("gamma", STRATEGY_PARAM_TYPE_STARTUP, VALUE_TYPE_DOUBLE, gamma));
    params().CreateParam(CreateStrategyParamArgs("noise_std", STRATEGY_PARAM_TYPE_STARTUP, VALUE_TYPE_DOUBLE, noise_std));
    params().CreateParam(CreateStrategyParamArgs("volume_clip", STRATEGY_PARAM_TYPE_STARTUP, VALUE_TYPE_DOUBLE, volume_clip));
    params().CreateParam(CreateStrategyParamArgs("window_size", STRATEGY_PARAM_TYPE_STARTUP, VALUE_TYPE_INT, window_size)); 
    params().CreateParam(CreateStrategyParamArgs("normalise_factor", STRATEGY_PARAM_TYPE_STARTUP, VALUE_TYPE_DOUBLE, normalise_factor));    
    params().CreateParam(CreateStrategyParamArgs("normalise_window_size", STRATEGY_PARAM_TYPE_STARTUP, VALUE_TYPE_INT, normalise_window_size));
    params().CreateParam(CreateStrategyParamArgs("vol_window_size", STRATEGY_PARAM_TYPE_STARTUP, VALUE_TYPE_INT, vol_window_size));
    params().CreateParam(CreateStrategyParamArgs("n_parameters", STRATEGY_PARAM_TYPE_STARTUP, VALUE_TYPE_INT, n_parameters));
    params().CreateParam(CreateStrategyParamArgs("lot_size", STRATEGY_PARAM_TYPE_STARTUP, VALUE_TYPE_INT, lot_size));
    params().CreateParam(CreateStrategyParamArgs("memory_size", STRATEGY_PARAM_TYPE_STARTUP, VALUE_TYPE_INT, memory_size));
    params().CreateParam(CreateStrategyParamArgs("least_count", STRATEGY_PARAM_TYPE_STARTUP, VALUE_TYPE_DOUBLE, least_count));
    params().CreateParam(CreateStrategyParamArgs("batch_size", STRATEGY_PARAM_TYPE_STARTUP, VALUE_TYPE_INT, batch_size));
    params().CreateParam(CreateStrategyParamArgs("new_model", STRATEGY_PARAM_TYPE_STARTUP, VALUE_TYPE_BOOL, new_model));
    params().CreateParam(CreateStrategyParamArgs("eval", STRATEGY_PARAM_TYPE_STARTUP, VALUE_TYPE_BOOL, eval));
}

void DDPGStrategy::DefineStrategyCommands(){}

// By default, SS will register to trades/quotes/depth data for the instruments you have requested via command_line.
void DDPGStrategy::RegisterForStrategyEvents(StrategyEventRegister* eventRegister, DateType currDate){

    if (!params().GetParam("n_parameters")->Get(&n_parameters) or !params().GetParam("window_size")->Get(&window_size)){
        cout << "Error getting n_parameters or window_size parameter" << endl;
        return;
    }
    state_size = n_parameters*window_size;
    n_actions = 2;
    eventRegister->RegisterForSingleScheduledEvent("terminate strategy",USEquityCloseUTCTime(currDate)-boost::posix_time::seconds(10));
    eventRegister->RegisterForRecurringScheduledEvents("hourly_ping", USEquityOpenUTCTime(currDate), USEquityCloseUTCTime(currDate), boost::posix_time::hours(1));
}

void DDPGStrategy::OnTrade(const TradeDataEventMsg& msg) {}

void DDPGStrategy::OnScheduledEvent(const ScheduledEventMsg& msg) {
    if (msg.scheduled_event_name() == "hourly_ping"){
        for (InstrumentSetConstIter it = instrument_begin(); it != instrument_end(); ++it) {
        const Instrument* instrument = it->second;
        RecordAccountStats(instrument);
        }
    }
    if (msg.scheduled_event_name() == "day_end"){
        cout << "Day end" << endl;
        strategy_active_bool = false;
        for (InstrumentSetConstIter it = instrument_begin(); it != instrument_end(); ++it) {
            const Instrument* instrument = it->second;
            done_flag[instrument]=true;
            StrategyLoop(instrument);
        }
    }
}

void DDPGStrategy::OnOrderUpdate(const OrderUpdateEventMsg& msg) {}

void DDPGStrategy::OnBar(const BarEventMsg& msg){
}

void DDPGStrategy::OnQuote(const QuoteEventMsg& msg){
    if (strategy_active_bool and CheckBidAskChange(&msg.instrument()){
        const Instrument* instrument = &msg.instrument();
        StrategyLoop(instrument);
    }
}

bool DDPGStrategy::CheckBidAskChange(const Instrument* instrument){
    const IAggrOrderBook &orderBook = instrument->aggregate_order_book();
    double ask = orderBook.BestAskLevel()->price();
    double bid = orderBook.BestBidLevel()->price();
    if (previous_bid[instrument] == bid and previous_ask[instrument] == ask){
        return false;
    }
    else{
        previous_bid[instrument] = bid;
        previous_ask[instrument] = ask;
        return true;
    }
}

void DDPGStrategy::StrategyLoop(const Instrument* instrument){
    if (eval){
        EvaluateLoop(instrument);
    }
    else{
        TrainingLoop(instrument);
    }
}

void DDPGStrategy::TrainingLoop(const Instrument* instrument){
    // checks if all state tracking vectors are initialised
    CheckAndInitialiseVectors(instrument);
    // updates the data for the instrument
    UpdateData(instrument);
    // checks if all data is initialised
    if (!(DataFullyInitialised(instrument))){
        return;
    }
    // generates the state for the instrument
    Generate1DState(instrument);
    if (!(state_map[instrument].FullyInitialized())){
        return;
    }

    // gets the previous state upon which the action was taken.
    vector<double> current_state = prev_state[instrument];
    cout << "Current state is " << current_state << endl;
    //gets the current state as the future state for the experience.
    vector<double> next_state = state_map[instrument].GetState();
    cout << "Next state is " << next_state << endl;
    // updates the previous state to the current state
    prev_state[instrument] = next_state;
    // calculates the reward for the action taken previously
    double current_pnl = portfolio().total_pnl();
    double reward = current_pnl - previous_pnl[instrument];
    previous_pnl[instrument] = portfolio().total_pnl();
    // checks if the episode is done
    bool done = done_flag[instrument];
    // adds the experience to the replay buffer
    replay_buffer_map[instrument].addExperience(current_state, previous_action[instrument], reward, next_state, done, noise_std_map[instrument]);
    // gets the next action to be taken 
    pair<double, double> action = GetAction(instrument,next_state);
    cout << "Action is " << action << endl;
    // cout << "Action is " << action << endl;
    // updates the previous action to the current action
    previous_action[instrument] = action;
    // takes the current action
    pair<double, double> prices = ConvertActions(action, instrument);
    cout << "Prices are " << prices << endl;
    if (done_flag[instrument]){
        //liquidateAll(instrument);
        cout << "entered liquidation" << endl;
    }
    else{
        updateOrderPositions(instrument, prices);
    }


    // learning step
    int batch_size;
    if (!params().GetParam("batch_size")->Get(&batch_size)){
        cout << "Error getting batch size parameter" << endl;
        return;
    }
    if (replay_buffer_map[instrument].getSize() > batch_size){
        // cout << "Learning from batch" << endl;
        std::vector<Experience> batch = replay_buffer_map[instrument].getBatch(batch_size);
        // replay_buffer_map[instrument].debug_batch(batch, false);
        ddpg_agent_map[instrument].learn(batch);
    }
}

void DDPGStrategy::EvaluateLoop(const Instrument* instrument){
    // checks if all state tracking vectors are initialised
    CheckAndInitialiseVectors(instrument);
    // updates the data for the instrument
    UpdateData(instrument);
    // checks if all data is initialised
    if (!(DataFullyInitialised(instrument))){
        return;
    }
    // generates the state for the instrument
    Generate1DState(instrument);
    if (!(state_map[instrument].FullyInitialized())){
        return;
    }

    vector<double> current_state = state_map[instrument].GetState();
    pair<double, double> action = GetAction(instrument,current_state);
    // updates the previous action to the current action
    previous_action[instrument] = action;
    // takes the current action
    pair<double, double> prices = ConvertActions(action, instrument);
    // updates the previous action to the current action
    previous_action[instrument] = action;
    // takes the current action
    if (done_flag[instrument]){
        liquidateAll(instrument);
    }
    else{
        updateOrderPositions(instrument, prices);
    }
}
void DDPGStrategy::OnMarketState(const MarketStateEventMsg& msg){}

void DDPGStrategy::OnResetStrategyState(){}

void DDPGStrategy::OnParamChanged(StrategyParam& param) {}