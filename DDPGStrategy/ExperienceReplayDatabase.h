#include <string>
#include <sstream>
#include <unordered_map>
#include <filesystem>
#include <iostream>
#include <algorithm>
#include <cmath>
#include <memory>
#include <vector>
#include <sqlite_modern_cpp.h>
#include <stdexcept>
#include <cstdlib> // For rand(), srand()
#include <ctime>   // For time()
#include <unordered_set> 
#include "experience.h"
using namespace std;

struct episode_info {
    int count;
    double noise_std;
};
class ExperienceReplayDatabase {
public:
    
    // Default constructor
    ExperienceReplayDatabase() : db_path(""), db(nullptr), max_rows(0) {}

    // Existing constructor
    ExperienceReplayDatabase(const std::string& db_path, int max_rows = 10000, bool new_model = true)
        : db_path(db_path), max_rows(max_rows){
        // Extract the directory from db_path
        auto directory = std::filesystem::path(db_path).parent_path();
        // Create directory if it doesn't exist
        if (new_model){
            if (!directory.empty() && !std::filesystem::exists(directory)) {
                std::filesystem::create_directories(directory);
            }
            counter = 0;
            cout << "counter set to " << counter << endl;
            db = std::make_unique<sqlite::database>(db_path);

            std::string drop_table = "DROP TABLE IF EXISTS experience;";

            std::string drop_store = "DROP TABLE IF EXISTS store;";

            std::string create_table =
                "CREATE TABLE experience ("
                "id INTEGER, "
                "state TEXT, "
                "action TEXT, "
                "reward REAL, "
                "next_state TEXT, "
                "done INTEGER, "
                "created_at DATETIME DEFAULT CURRENT_TIMESTAMP);"; // Added timestamp column with default current timestamp

            std::string create_store = 
            "CREATE TABLE store ("
                "id INTEGER PRIMARY KEY, "
                "count INTEGER, "
                "noise_std REAL);";
            std::string insert_store = "INSERT INTO store (id, count, noise_std) VALUES (1, 0, 1.0);";

            try {
                (*db) << drop_table;
                (*db) << drop_store;
            } catch (const sqlite::sqlite_exception &e) {
                std::cerr << "Error dropping table: " << e.what() << std::endl;
                throw;
            }

            try {
                (*db) << create_table;
                (*db) << create_store;
                (*db) << insert_store;
            } catch (const sqlite::sqlite_exception &e) {
                std::cerr << "Error creating Table: " << e.what() << std::endl;
                throw;
            }
        }
        else {
            db = std::make_unique<sqlite::database>(db_path);
            std::string get_counter = "SELECT count from store;";
            try {
                (*db) << get_counter >> counter;
                cout << "counter set to " << counter << endl;
            }catch (const sqlite::sqlite_exception &e) {
                std::cerr << "Error getting count from store: " << e.what() << std::endl;
                throw;
            }
        }
    }

    // Copy constructor (deleted)
    ExperienceReplayDatabase(const ExperienceReplayDatabase&) = delete;

    // Copy assignment operator (deleted)
    ExperienceReplayDatabase& operator=(const ExperienceReplayDatabase&) = delete;

    // Move constructor (with max_rows)
    ExperienceReplayDatabase(ExperienceReplayDatabase&& other) noexcept
        : db(std::move(other.db)), db_path(std::move(other.db_path)), max_rows(other.max_rows),counter(other.counter) {}

    // Move assignment operator (with max_rows)
    ExperienceReplayDatabase& operator=(ExperienceReplayDatabase&& other) noexcept {
        if (this != &other) {
            db = std::move(other.db);
            db_path = std::move(other.db_path);
            max_rows = other.max_rows;
            counter = other.counter;
        }
        return *this;
    }

    void addExperience(std::vector<double> state, pair<double,double> action, float reward, std::vector<double> next_state, bool done, double noise_std) {
        std::string state_str = serializeState(state);
        std::string next_state_str = serializeState(next_state);
        std::string action_str = serializeAction(action);
        int done_int = done ? 1 : 0;
        counter = counter%max_rows + 1;
        std::string query = "INSERT INTO experience (id, state, action, reward, next_state, done) "
                                       "VALUES (?, ?, ?, ?, ?, ?);";
        std::string counter_query = "UPDATE store SET count = ?, noise_std = ? WHERE id = 1;";
        try {
            auto stmt = (*db) << query;
            stmt <<  counter  << state_str << action_str << reward << next_state_str << done_int;
            stmt.execute();

            int count = getSize();
            if (count > max_rows) {
                // Delete the oldest row with the same id
                std::string delete_query = "DELETE FROM experience WHERE id = ? AND rowid = ("
                                        "SELECT rowid FROM experience WHERE id = ? ORDER BY created_at ASC LIMIT 1);";
                try {
                    (*db) << delete_query << counter << counter;
                } catch (const sqlite::sqlite_exception &e) {
                    std::cerr << "Failed to delete old experience: " << e.what() << std::endl;
                    throw;
                }
            }
            auto stmt2 = (*db) << counter_query;
            stmt2 << counter << noise_std;
            stmt2.execute();

        } catch (const sqlite::sqlite_exception &e) {
            std::cerr << "Failed to insert experience: " << e.what() << std::endl;
        }
    }

    int getSize() {
        int count = 0;
        try {
            (*db) << "SELECT COUNT(*) FROM experience;" >> count;
        } catch (const std::exception &e) {
            std::cerr << "Failed to get size: " << e.what() << std::endl;
            throw;
        }
        return count;
    }

    std::vector<Experience> getBatch(int batch_size) {
        std::vector<Experience> batch;
        int count = getSize();

        if (count == 0) {
            throw std::runtime_error("No experiences in the database.");
        }

        if (batch_size > count) {
            throw std::invalid_argument("Batch size exceeds number of experiences in the database.");
        }

        try {
            srand(static_cast<unsigned int>(time(nullptr)));  // Seed random number generator
            std::unordered_set<int> chosen_ids;
            while (chosen_ids.size() < std::min(static_cast<int>(batch_size), count)) {
                int random_id = rand() % count + 1; // Generate random ID between 1 and count
                chosen_ids.insert(random_id);
            }

            for (int id : chosen_ids) {
                std::string state_str, next_state_str, action;
                float reward;
                int done_int;
                (*db) <<
                    "SELECT state, action, reward, next_state, done FROM experience WHERE id = ?;" <<
                    id >>
                    [&](std::string state, std::string action, float reward, std::string next_state, int done) {
                        vector<double> state_v = deserializeState(state);
                        vector<double> next_state_v = deserializeState(next_state);
                        pair <double, double> action_p = deserializeAction(action);
                        batch.emplace_back(state_v, action_p, reward, next_state_v, done);
                    };
            }
        } catch (const std::exception& e) {
            std::cerr << "Failed to fetch batch: " << e.what() << std::endl;
            throw;
        }
        return batch;
    }

        // Function to print a vector of doubles
    void print_vector(const std::vector<double>& v) {
        for (auto& element : v) {
            std::cout << element << " ";
        }
        std::cout << std::endl;
    }

    // Function to display the contents of the batch
    void debug_batch(const std::vector<Experience>& batch, bool print_all = false) {
        std::cout << "Total number of experiences: " << batch.size() << std::endl;
        std::cout << "-----------------------" << std::endl;
        if (print_all){
            int count = 1;
            for (const auto& experience : batch) {
                std::cout << "Experience " << count++ << ":" << std::endl;
                std::cout << "Vector1: ";
                print_vector(std::get<0>(experience));
                std::cout << "Pair1: " << std::get<1>(experience) << std::endl;
                std::cout << "Float: " << std::get<2>(experience) << std::endl;
                std::cout << "Vector2: ";
                print_vector(std::get<3>(experience));
                std::cout << "Int2: " << std::get<4>(experience) << std::endl;
                std::cout << "-----------------------" << std::endl;
            }
        }
    }

    episode_info getEpisodeInfo() {
        episode_info info;
        try {
            (*db) << "SELECT count, noise_std FROM store WHERE id = 1;" >> [&](int count, double noise_std) {
                info.count = count;
                info.noise_std = noise_std;
            };
        } catch (const std::exception &e) {
            std::cerr << "Failed to get episode info: " << e.what() << std::endl;
            throw;
        }
        return info;
    }

private:
    std::unique_ptr<sqlite::database> db;
    std::string db_path;
    int max_rows;
    int counter;

    std::string serializeState(vector<double> state) {
        std::ostringstream oss;
        if (!state.empty()) {
            // Convert each element to string and concatenate with a separator
            std::copy(state.begin(), state.end() - 1, std::ostream_iterator<float>(oss, ","));
            // Add the last element without a trailing comma
            oss << state.back();
        }
        return oss.str();
    }

    vector<double> deserializeState(const std::string& state_str) {
        std::istringstream iss(state_str);
        vector<double> state;
        std::string value;
        while (std::getline(iss, value, ',')) {
            state.push_back(std::stof(value));
        }
        return state;
    }

    std::string serializeAction(pair <double, double> action) {
        std::ostringstream oss;
        oss << action.first << "," << action.second;
        return oss.str();
    }

    pair<double, double> deserializeAction(const std::string& action_str) {
        std::istringstream iss(action_str);
        double first, second;
        iss >> first;
        iss.ignore(1); // Ignore the comma
        iss >> second;
        return std::make_pair(first, second);
    }
    
};