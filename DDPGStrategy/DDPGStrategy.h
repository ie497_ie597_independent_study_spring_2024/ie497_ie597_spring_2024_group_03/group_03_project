// This is a preprocessor directive that ensures that the header file is included only once in a given compilation unit, to avoid multiple definitions.
#pragma once

// These are include guards that prevent redefinition of class names, macro constants, and typedef names. 
// Include guards help avoiding name conflicts in large software projects.
#ifndef DDPGStrategy_H
#define DDPGStrategy_H

// This is a conditional preprocessor directive that defines a macro _STRATEGY_EXPORTS as __declspec(dllexport) on Windows platform, and empty on other platforms.
// This macro is used to export the HelloworldStrategy class to the dynamic link library (DLL) that is loaded by the trading engine.
#ifdef _WIN32
    #define _STRATEGY_EXPORTS __declspec(dllexport)
#else
    #ifndef _STRATEGY_EXPORTS
    #define _STRATEGY_EXPORTS
    #endif
#endif

/**
 * Below are header files that are used by the HelloworldStrategy class. We just tell the compiler to look for these files.
 * You will not have Strategy.h & Instrument.h in your directory. These are part of the SDK.
 * Strategy.h is the main header file for the strategy development kit and provides access to the core functionality of the trading engine.
 * Instrument.h is a header file for instrument specific data. 
 * The remaining headers provide various utility functions.
**/
#include <Strategy.h>
#include <MarketModels/Instrument.h>
#include <MarketModels/IAggrOrderBook.h>
#include <Analytics/ScalarRollingWindow.h>
#include <Analytics/InhomogeneousOperators.h>
#include <Analytics/IncrementalEstimation.h>
#include <Utilities/ParseConfig.h>
#include <IPositionRecord.h>
#include <Order.h>
#include <BarDataTypes.h>
#include <IOrderTracker.h>

#include <string>
#include <unordered_map>
#include <iostream>
#include <algorithm> 
#include <cmath>
#include <memory>
#include <vector>
#include <deque>
#include <random>
// Class declaration
#include <sqlite_modern_cpp.h>
#include <torch/torch.h>
#include "ExperienceReplayDatabase.h"
#include "Agent.h"

// Import namespace RCM::StrategyStudio to avoid explicit namespace qualification when using names from this namespace
using namespace RCM::StrategyStudio;
using namespace RCM::StrategyStudio::Utilities;
using namespace RCM::StrategyStudio::MarketModels;
using namespace std;

class SpreadZScore
{
public:
    SpreadZScore() : window(0), normalise_factor(0.0) {} 
    SpreadZScore(int window_size, double normalise_factor) : window(window_size), normalise_factor(normalise_factor) {}

    void Reset()
    {
        window.clear();
    }

    void Update(double val)
    {
        window.push_back(val);
    }

    double GetSpread(){
        return clipAndScale(window.ZScore());
    }

    double clipAndScale(double value) {
        return max(-normalise_factor, min(value, normalise_factor))/ normalise_factor;
    }

    bool FullyInitialized() { return window.full(); }

private:
    Analytics::ScalarRollingWindow<double> window;
    double normalise_factor;
};

class VolatilityZScore
{
public:
    VolatilityZScore() : price_window(0),vol_window(0), normalise_factor(0.0) {} 
    VolatilityZScore(int vol_window_size, int normalise_window_size, double normalise_factor) : price_window(vol_window_size),vol_window(normalise_window_size),normalise_factor(normalise_factor) {}
    void Reset()
    {
        price_window.clear();
        vol_window.clear();
    }

    void UpdateVol(double val)
    {
        price_window.push_back(val);
        if (PriceFullyInitialized()){
            vol_window.push_back(price_window.StdDev());
        }
    }

    double GetVol(){
        return clipAndScale(vol_window.ZScore());
    }

    double clipAndScale(double value) {
        return max(-normalise_factor, min(value, normalise_factor))/ normalise_factor;
    }

    bool PriceFullyInitialized() { return price_window.full(); }

private:
    Analytics::ScalarRollingWindow<double> price_window;
    Analytics::ScalarRollingWindow<double> vol_window;
    double normalise_factor;
};

// class StateMap2D
// {
// public:
//     StateMap2D() : window_size(0) {}
//     StateMap2D(int window_size) : window_size(window_size) {}
    
//     void Reset()
//     {
//         state_map.clear();
//     }

//     std::vector<torch::Tensor> UpdateState(torch::Tensor val)
//     {
//         // Add the new value to the window
//         if (state_map.size() == window_size) {
//             // If the window is full, remove the oldest element first
//             state_map.pop_front();
//         }
//         state_map.push_back(val);

//         // Return a vector containing all tensors in the current window
//         return std::vector<torch::Tensor>(state_map.begin(), state_map.end());
//     }

//     bool FullyInitialized() const
//     {
//         return state_map.size() == window_size;
//     }

// private:
//     std::deque<torch::Tensor> state_map;
//     int window_size;
// };

class StateMap1D
{
public:
    StateMap1D() : state_map(0) {}
    StateMap1D(int window_size, int n_parameters) : state_map(window_size * n_parameters) {}
    void Reset()
    {
        state_map.clear();
    }

    void UpdateState(vector<double> val_vector)
    {
        for (auto const& x : val_vector){
            state_map.push_back(x);
        }
    }

    vector<double> GetState(){
        vector<double> state_vector;
        for (auto const& x : state_map){
            state_vector.push_back(x);
        }
        return state_vector;
    }

    bool FullyInitialized() { return state_map.full(); }

private:
    Analytics::ScalarRollingWindow<double> state_map;
};

class ReturnsZScore
{
public:
    ReturnsZScore() : window(0), last_val(0), normalise_factor(0.0) {}
    ReturnsZScore(int window_size, double normalise_factor) : window(window_size),last_val(0),normalise_factor(normalise_factor) {}
    void Reset()
    {
        window.clear();
    }
    void Update(double val)
    {   
        if (last_val == 0){
            last_val = val;
        }
        window.push_back((val-last_val)/last_val);
    }

    double GetReturn(){
        return clipAndScale(window.ZScore());
    }

    double clipAndScale(double value) {
        return max(-normalise_factor, min(value, normalise_factor))/ normalise_factor;
    }

    bool FullyInitialized() { return window.full(); }

private:
    Analytics::ScalarRollingWindow<double> window;
    double last_val;
    double normalise_factor;
};

class DDPGStrategy : public Strategy {
public:
    // defining the spread map
    typedef boost::unordered_map<const Instrument*, SpreadZScore> SpreadMap; 
    typedef SpreadMap::iterator SpreadMapIterator;

    // defining the volatility map
    typedef boost::unordered_map<const Instrument*, VolatilityZScore> VolatilityMap;
    typedef VolatilityMap::iterator VolatilityMapIterator;

    typedef boost::unordered_map<const Instrument*, ReturnsZScore> ReturnsMap;
    typedef ReturnsMap::iterator ReturnsMapIterator;

    typedef boost::unordered_map<const Instrument*, StateMap1D> StateMapObject;
    typedef StateMapObject::iterator StateMapIterator;

    typedef boost::unordered_map<const Instrument*, ExperienceReplayDatabase> ReplayBufferMap;
    typedef ReplayBufferMap::iterator ReplayBufferMapIterator;

    typedef boost::unordered_map<const Instrument*, DDPGAgent> DDPGAgentMap;
    typedef DDPGAgentMap::iterator DDPGAgentMapIterator;
    
public:
    // Constructor & Destructor functions for this class
    DDPGStrategy(StrategyID strategyID,
        const std::string& strategyName,
        const std::string& groupName);
    ~DDPGStrategy();

public:

    // Below are event handling functions the user can override on the cpp file to create their own strategies.
    // Polymorphic behavior is achieved in C++ by using virtual functions, which allows the same function to behave differently depending on the type of object it is called on.

    /**
    * Called whenever a bar event occurs for an instrument that the strategy is subscribed to.
    * A bar event is a notification that a new bar has been formed in the price data of the instrument, where a bar represents a fixed period of time (e.g., 1 minute, 5 minutes, 1 hour) and contains information such as the opening and closing price, highest and lowest price, volume for that period.
    * The msg parameter of the OnBar function is an object of type BarEventMsg that contains information about the bar event that occurred.
    **/
    virtual void OnBar(const BarEventMsg& msg);

    /**
    * Called whenever a trade event occurs for an instrument that the strategy is subscribed to.
    * A trade event refers to a specific occurrence related to a trade of a financial instrument, such as a stock or a commodity like the execution of a buy or sell order
    * The msg parameter is an object of type TradeDataEventMsg that contains information about the trade event that occurred
    */
    virtual void OnTrade(const TradeDataEventMsg& msg);

    /**
     * This event triggers whenever a new quote for a market center arrives from a consolidate or direct quote feed,
     * or when the market center's best price from a depth of book feed changes.
     *
     * User can check if quote is from consolidated or direct, or derived from a depth feed. This will not fire if
     * the data source only provides quotes that affect the official NBBO, as this is not enough information to accurately
     * mantain the state of each market center's quote.
     */ 
    virtual void OnQuote(const QuoteEventMsg& msg);

    /**
     * This event triggers whenever a order book message arrives. This will be the first thing that
     * triggers if an order book entry impacts the exchange's DirectQuote or Strategy Studio's TopQuote calculation.
     */ 
    virtual void OnDepth(const MarketDepthEventMsg& msg){}

    /**
     * Called when a scheduled event occurs during the backtesting process.
     * Examples of actions include making trading decisions, adjusting parameters or indicators, updating strategy state, or triggering specific actions at predefined intervals and time-dependent trading strategies.
    */
    virtual void OnScheduledEvent(const ScheduledEventMsg& msg);

    // Called whenever there is an update to one of the orders placed by the strategy
    void OnOrderUpdate(const OrderUpdateEventMsg& msg);
 
    void OnResetStrategyState();

    void OnParamChanged(StrategyParam& param);

private:

    virtual void RegisterForStrategyEvents(StrategyEventRegister* eventRegister, DateType currDate);

    virtual void DefineStrategyParams();

    virtual void DefineStrategyCommands();

    void OnMarketState(const MarketStateEventMsg& msg);

    void StrategyLoop(const Instrument* instrument);

    void EvaluateLoop(const Instrument* instrument);

    void TrainingLoop(const Instrument* instrument);

    bool CheckBidAskChange(const Instrument* instrument);
    
    void CheckAndInitialiseVectors(const Instrument* instrument){
        double volume_clip;
        int window_size;
        double normalise_factor;
        int normalise_window_size;
        int vol_window_size;
        int n_parameters;
        int memory_size;
        double gamma;
        double noise_std;
        bool new_model;

        if (!(params().GetParam("volume_clip")->Get(&volume_clip)) or 
        !(params().GetParam("window_size")->Get(&window_size)) or 
        !(params().GetParam("normalise_factor")->Get(&normalise_factor)) or 
        !(params().GetParam("normalise_window_size")->Get(&normalise_window_size)) or 
        !(params().GetParam("vol_window_size")->Get(&vol_window_size)) or 
        !(params().GetParam("n_parameters")->Get(&n_parameters)) or 
        !(params().GetParam("memory_size")->Get(&memory_size)) or 
        !(params().GetParam("gamma")->Get(&gamma)) or 
        !(params().GetParam("noise_std")->Get(&noise_std)) or 
        !(params().GetParam("new_model")->Get(&new_model))){
            cout << "Error getting parameters" << endl;
            return;
        }
        int state_size = n_parameters*window_size;
        // int state_size = n_parameters*window_size;
        if (spread_map.find(instrument) == spread_map.end()){
            spread_map[instrument] = SpreadZScore(window_size,normalise_factor);
        }
        if (volatility_map.find(instrument) == volatility_map.end()){
            volatility_map[instrument] = VolatilityZScore(vol_window_size,normalise_window_size,normalise_factor);
        }
        if (returns_map.find(instrument) == returns_map.end()){
            returns_map[instrument] = ReturnsZScore(window_size,normalise_factor);
        }
        if (state_map.find(instrument) == state_map.end()){
            state_map[instrument] = StateMap1D(window_size,n_parameters);
        }
        if (replay_buffer_map.find(instrument) == replay_buffer_map.end()){
            cout << "new_model is " << new_model << endl;
            string instrument_db = "/groupstorage/group01/DDPGStrategy/dbs/" + instrument->symbol() + "_memory.db";
            replay_buffer_map[instrument] = ExperienceReplayDatabase(instrument_db,memory_size,new_model);
        }
        if (ddpg_agent_map.find(instrument) == ddpg_agent_map.end()){
            ddpg_agent_map[instrument] = DDPGAgent(state_size,n_parameters,0.001,gamma,tau,instrument->symbol(), new_model);
            ddpg_agent_map[instrument].print_agent_model_summary();
        }

        if (prev_state.find(instrument) == prev_state.end()){
            prev_state[instrument] = vector<double>(state_size,0.0);
        }
        if (previous_pnl.find(instrument) == previous_pnl.end()){
            previous_pnl[instrument] = 0.0;
        }
        if (previous_action.find(instrument) == previous_action.end()){
            previous_action[instrument] = pair<double,double>(0.0,0.0);
        }
        if (!(new_model))
            noise_std = replay_buffer_map[instrument].getEpisodeInfo().noise_std;
        if (noise_std_map.find(instrument) == noise_std_map.end()){
            noise_std_map[instrument] = noise_std;
        }
        if (done_flag.find(instrument) == done_flag.end()){
            done_flag[instrument] = false;
        }
        if (previous_bid.find(instrument) == previous_bid.end()){
            previous_bid[instrument] = 0.0;
        }
        if (previous_ask.find(instrument) == previous_ask.end()){
            previous_ask[instrument] = 0.0;
        }
    }

    void UpdateData(const Instrument* instrument){
        const IAggrOrderBook &orderBook = instrument->aggregate_order_book();
        double mid_price = CalculateMidPrice(orderBook);
        double spread = CalculateSpread(orderBook);
        spread_map[instrument].Update(spread);
        volatility_map[instrument].UpdateVol(mid_price);
        returns_map[instrument].Update(mid_price);
    }

    bool DataFullyInitialised(const Instrument* instrument){
        return spread_map[instrument].FullyInitialized() && volatility_map[instrument].PriceFullyInitialized() && returns_map[instrument].FullyInitialized();
    }

    double CalculateImbalance(const IAggrOrderBook& orderBook){
        double ask_size = static_cast<double>(orderBook.TotalAskSize());
        double bid_size = static_cast<double>(orderBook.TotalBidSize());
        return (ask_size - bid_size) / (ask_size + bid_size);
    }

    double CalculateMidPrice(const IAggrOrderBook& orderBook) {
        return (orderBook.BestAskLevel()->price() + orderBook.BestBidLevel()->price()) / 2;
    }

    double CalculateSpread(const IAggrOrderBook& orderBook){
        return orderBook.BestAskLevel()->price() - orderBook.BestBidLevel()->price();
    }

    // map< double, double> GenerateDepthDict(const IAggrOrderBook& orderBook){
    //     map< double, double> depth_dict;
    //     double mid_price = CalculateMidPrice(orderBook);
    //     int i = 0;
    //     for (const IAggrPriceLevel* level = orderBook.BestBidLevel(); i <= 10; level = level->next()){
    //         if (level == nullptr){
    //             double lowest_level = depth_dict.begin()->first;
    //             depth_dict[lowest_level + (0.01/mid_price)] = 0.0;
    //         }
    //         else{
    //             depth_dict[level->price()/mid_price] = max(level->size()/volume_clip,1.0);
    //         }
    //     }
    //     for (const IAggrPriceLevel* level = orderBook.BestAskLevel(); i <= 10; level = level->next()){
    //         if (level == nullptr){
    //             double highest_level = depth_dict.rbegin()->first;
    //             depth_dict[highest_level + (0.01/mid_price)] = 0.0;
    //         }
    //         else{
    //             depth_dict[level->price()/mid_price] = -max(level->size()/volume_clip,1.0);
    //         }
    //     }
    //     return depth_dict;
    // }

    void Generate1DState(const Instrument* instrument){
        const IAggrOrderBook &orderBook = instrument->aggregate_order_book();
        double imbalance = CalculateImbalance(orderBook);
        double spread_state = spread_map[instrument].GetSpread();
        double volatility_state = volatility_map[instrument].GetVol();
        double returns_state = returns_map[instrument].GetReturn();

        vector<double> state_vector;

        state_vector.push_back(imbalance);
        state_vector.push_back(spread_state);
        state_vector.push_back(volatility_state);
        state_vector.push_back(returns_state);
        state_map[instrument].UpdateState(state_vector);
    }

    // std::pair<double, double> generateActions() {
    //     // Initialize a random number generator
    //     std::random_device rd;
    //     std::mt19937 gen(rd());
    //     std::uniform_real_distribution<double> dis(-1.0, 1.0);

    //     // Generate the first random value
    //     double val1 = dis(gen);

    //     // Generate the second random value within the range [-1, 1 - val1]
    //     double val2 = dis(gen);
    //     if (val1 + val2 > 1.0) {
    //         val2 = 1.0 - val1;
    //     }

    //     return {val1, val2};
    // }

    std::pair<double, double> GetAction(const Instrument* instrument, const std::vector<double>& next_state){
        // Retrieve the action from the agent
        std::pair<double, double> actions = ddpg_agent_map[instrument].get_action(next_state, noise_std_map[instrument]);
        return actions;
    }

    std::pair<double,double> ConvertActions(std::pair<double, double> actions, const Instrument* instrument){
        const IAggrOrderBook &orderBook = instrument->aggregate_order_book();
        double spread = CalculateSpread(orderBook);
        double ask = orderBook.BestAskLevel()->price();
        double bid = orderBook.BestBidLevel()->price();
        double least_count;
        if (!(params().GetParam("least_count")->Get(&least_count))){
            cout << "least_count not found" << endl;
            return {bid, ask};
        }
        double order_bid = bid + round((actions.first * spread) / least_count) * least_count;
        double order_ask = ask - round((actions.second * spread) / least_count) * least_count;
        return {order_bid, order_ask};
    }


    // BidAsk ConvertActions(int action, const Instrument* instrument){
    //     const IAggrOrderBook &orderBook = instrument->aggregate_order_book();
    //     double bid_price = orderBook.BestBidLevel()->price();
    //     double ask_price = orderBook.BestAskLevel()->price();

    //     int bid_action = action / ask_steps;
    //     int ask_action = action % ask_steps;
    //     double order_ask_price = ask_price - (least_count * bid_action);
    //     double order_bid_price = bid_price + (least_count * ask_action);
    //     BidAsk bid_ask = {order_bid_price, order_ask_price};
    //     return bid_ask;
    // }

    void updateOrderPositions(const Instrument* instrument,std::pair<double,double> actions){
        bool ask = false;
        bool bid = false;
        int lot_size;
        if (!(params().GetParam("lot_size")->Get(&lot_size))){
            cout << "lot_size not found" << endl;
            return;
        }
        OrderParams bid_params(*instrument,
                        lot_size,
                        actions.first,
                        MARKET_CENTER_ID_NASDAQ,
                        ORDER_SIDE_BUY,
                        ORDER_TIF_DAY,
                        ORDER_TYPE_LIMIT);
        OrderParams ask_params(*instrument,
                        lot_size,
                        actions.second,
                        MARKET_CENTER_ID_NASDAQ,
                        ORDER_SIDE_SELL_SHORT,
                        ORDER_TIF_DAY,
                        ORDER_TYPE_LIMIT);
        // Check if there are any working orders and update them
        for (auto it = orders().working_orders_begin(instrument); it != orders().working_orders_end(instrument); ++it){
            Order* order = *it;
            OrderParams param = order->params();
            OrderID id = order->order_id();
            if (param.order_side == ORDER_SIDE_SELL_SHORT)
            {
                trade_actions()->SendCancelReplaceOrder(id , ask_params);
                ask = true;
            }
            else{
                trade_actions()->SendCancelReplaceOrder(id , bid_params);
                bid = true;
            }
        }
        // If there are no working orders, send new orders
        if (!ask){
            trade_actions()->SendNewOrder(ask_params);
        }
        if (!bid){
            trade_actions()->SendNewOrder(bid_params);
        }
    }

    void liquidateAll(const Instrument* instrument){
        int inventory = portfolio().position(instrument);
        double price = (inventory > 0) ? instrument->top_quote().bid() - 10 : instrument->top_quote().ask() + 10;
        OrderSide action = (inventory > 0) ? ORDER_SIDE_SELL : ORDER_SIDE_BUY;

        OrderParams params_(*instrument,
                        abs(inventory),
                        price,
                        MARKET_CENTER_ID_NASDAQ,
                        action,
                        ORDER_TIF_DAY,
                        ORDER_TYPE_LIMIT);
        trade_actions()->SendNewOrder(params_);
    }

    bool noWorkingOrders(const Instrument* instrument){
        auto it = orders().working_orders_begin(instrument); 
        if (it == orders().working_orders_end(instrument)){
            return false;
        }
        return true;
    }

    void RecordAccountStats(const Instrument* instrument){
        cout << "instrument is " << instrument->symbol() << endl;
        cout << "Total PNL " << portfolio().total_pnl(instrument) << endl;
        cout << "Unrealised PNL " << portfolio().unrealized_pnl(instrument) << endl;
        cout << "Realised PNL " << portfolio().realized_pnl(instrument) << endl;
    }

private:
    
    //runtime parameters
    double tau;
    double gamma;
    double volume_clip;
    int window_size;
    double normalise_factor;
    int normalise_window_size;
    int vol_window_size;
    int n_parameters;
    int lot_size;
    double least_count;
    int batch_size;
    bool new_model;
    double noise_std;
    int memory_size;
    bool eval;
    bool strategy_active_bool;

    boost::unordered_map<const Instrument*, vector<double> > prev_state;
    boost::unordered_map<const Instrument*, double > previous_pnl;
    boost::unordered_map<const Instrument*, std::pair< double, double> > previous_action;
    boost::unordered_map<const Instrument*, double > noise_std_map;
    boost::unordered_map<const Instrument*, bool > done_flag;
    boost::unordered_map<const Instrument*, double > previous_bid;
    boost::unordered_map<const Instrument*, double > previous_ask;

    int n_actions;
    int state_size;
    SpreadMap spread_map;
    VolatilityMap volatility_map;
    ReturnsMap returns_map;
    StateMapObject state_map;
    ReplayBufferMap replay_buffer_map;
    DDPGAgentMap ddpg_agent_map;
};

// extern "C" is used to tell the compiler that these functions have C-style linkage instead of C++-style linkage, which means the function names will not be mangled.
// Except the strategy name, you don't need to change anything in this section.
extern "C" {

    _STRATEGY_EXPORTS const char* GetType() {
        return "DDPGStrategy";
    }

    _STRATEGY_EXPORTS IStrategy* CreateStrategy(const char* strategyType,
                                   unsigned strategyID,
                                   const char* strategyName,
                                   const char* groupName) {
        if (strcmp(strategyType, GetType()) == 0) {
            return *(new DDPGStrategy(strategyID, strategyName, groupName));
        } else {
            return NULL;
        }
    }

    _STRATEGY_EXPORTS const char* GetAuthor() {
        return "dlariviere";
    }

    _STRATEGY_EXPORTS const char* GetAuthorGroup() {
        return "UIUC";
    }

    _STRATEGY_EXPORTS const char* GetReleaseVersion() {
        return Strategy::release_version();
    }
}
// The #endif statement marks the end of the include guard to prevent the header file from being included multiple times.
#endif