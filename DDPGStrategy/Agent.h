#include <torch/torch.h>
#include <vector>
#include <string>
#include "ActorCritic.h"
#include <memory>
#include <random>
#include <algorithm>
#include <filesystem>
#include "experience.h"

class DDPGAgent {
private:
    std::shared_ptr<Actor> actor;
    std::shared_ptr<Actor> target_actor;
    std::shared_ptr<Critic> critic;
    std::shared_ptr<Critic> target_critic;
    std::shared_ptr<torch::optim::Adam> actor_optimizer;
    std::shared_ptr<torch::optim::Adam> critic_optimizer;
    double gamma;
    double tau; // Soft update coefficient
    int steps;
    std::string instrument_name;

public:
    // Default Constructor
    DDPGAgent()
        : actor(std::make_shared<Actor>(1,1)),
          target_actor(std::make_shared<Actor>(1,1)),
          critic(std::make_shared<Critic>(1,1)),
          target_critic(std::make_shared<Critic>(1,1)),
          gamma(0.99),
          tau(0.005),
          instrument_name("default"),
          steps(0) {
        std::cout << "Default DDPGAgent initialized" << std::endl;
    }

    // Parameterized constructor
    DDPGAgent(int state_dim, int action_dim, double lr = 0.001, double gamma = 0.99, double tau = 0.005, std::string instrument_name = "default", bool new_model = true)
        : actor(std::make_shared<Actor>(state_dim, action_dim)),
          target_actor(std::make_shared<Actor>(state_dim, action_dim)),
          critic(std::make_shared<Critic>(state_dim, action_dim)),
          target_critic(std::make_shared<Critic>(state_dim, action_dim)),
          gamma(gamma),
          tau(tau),
          instrument_name(instrument_name),
          steps(0) {
        if (!new_model) {
            std::cout << "Loading existing model..." << endl;
            load_model();
        }else{
            initialize_optimizer();
        }
        update_target_network(1.0);
        std::cout << "DDPGAgent initialized" << std::endl;
    }

    // Delete the copy constructor
    DDPGAgent(const DDPGAgent&) = delete;

    //Delete the copy assignment operator
    DDPGAgent& operator=(const DDPGAgent&) = delete;

    // Move constructor
    DDPGAgent(DDPGAgent&& other) noexcept
        : actor(std::move(other.actor)),
          target_actor(std::move(other.target_actor)),
          critic(std::move(other.critic)),
          target_critic(std::move(other.target_critic)),
          actor_optimizer(std::move(other.actor_optimizer)),
          critic_optimizer(std::move(other.critic_optimizer)),
          gamma(other.gamma),
          tau(other.tau),
          instrument_name(std::move(other.instrument_name)),
          steps(other.steps) {
        std::cout << "DDPGAgent moved" << std::endl;
    }

    // Move assignment operator
    DDPGAgent& operator=(DDPGAgent&& other) noexcept {
        if (this != &other) {
            actor = std::move(other.actor);
            target_actor = std::move(other.target_actor);
            critic = std::move(other.critic);
            target_critic = std::move(other.target_critic);
            actor_optimizer = std::move(other.actor_optimizer);
            critic_optimizer = std::move(other.critic_optimizer);
            gamma = other.gamma;
            tau = other.tau;
            instrument_name = std::move(other.instrument_name);
            steps = other.steps;
        }
        std::cout << "DDPGAgent moved" << std::endl;
        return *this;
    }

    void initialize_optimizer(double lr = 0.001) {
        actor_optimizer = std::make_shared<torch::optim::Adam>(actor->parameters(), torch::optim::AdamOptions(lr));
        critic_optimizer = std::make_shared<torch::optim::Adam>(critic->parameters(), torch::optim::AdamOptions(lr));
    }

    void update_target_network(double tau_t) {
        auto source_actor_params = actor->parameters();
        auto target_actor_params = target_actor->parameters();

        if (source_actor_params.size() != target_actor_params.size()) {
            throw std::runtime_error("Size mismatch between source and target network parameters");
        }
        for (size_t i = 0; i < source_actor_params.size(); ++i) {
            target_actor_params[i].data().copy_(tau_t * source_actor_params[i].data() + (1.0 - tau_t) * target_actor_params[i].data());
        }

        auto source_critic_params = critic->parameters();
        auto target_critic_params = target_critic->parameters();

        if (source_critic_params.size() != target_critic_params.size()) {
            throw std::runtime_error("Size mismatch between source and target network parameters");
        }
        for (size_t i = 0; i < source_critic_params.size(); ++i) {
            target_critic_params[i].data().copy_(tau_t * source_critic_params[i].data() + (1.0 - tau_t) * target_critic_params[i].data());
        }
        std::cout << "Target networks updated" << std::endl;
    }

    pair <double,double>  get_action(vector<double> state, double noise_std = 0.1) {
        actor->eval();
        vector<float> state_float = convertToFloat(state);
        torch::Tensor state_tensor = torch::from_blob(state_float.data(), {(int)state_float.size()}, torch::kFloat32).clone();
        torch::Tensor action = actor->forward(state_tensor);
        action = action + torch::randn_like(action) * noise_std;  // Exploration noise
        actor->train();
        torch::Tensor clipped_action = torch::clamp(action, -1.0, 1.0); // Assuming action space [-1, 1]
        return {clipped_action[0][0].item<double>(), clipped_action[0][1].item<double>()};
    }

    void learn(std::vector<Experience> batch) {
        if (!actor_optimizer or !critic_optimizer) {
            initialize_optimizer();
        }

        std::vector<torch::Tensor> states, actions, rewards, next_states, dones;

        for (const auto &exp : batch) {
            // Unpack the experience tuple
            vector<double> state = std::get<0>(exp);
            pair<double,double> action = std::get<1>(exp);
            double reward = std::get<2>(exp);
            vector<double> next_state = std::get<3>(exp);
            int done = std::get<4>(exp);

            // Convert std::vector<double> to std::vector<float> and then to torch::Tensor
            auto state_floats = convertToFloat(state);
            auto next_state_floats = convertToFloat(next_state);
            pair <float,float> action_float = {static_cast<float>(action.first), static_cast<float>(action.second)};
            auto action_tensor = torch::tensor({action_float.first, action_float.second}, torch::kFloat32);

            torch::Tensor state_tensor = torch::from_blob(state_floats.data(), {(int)state_floats.size()}, torch::kFloat32).clone();
            states.push_back(state_tensor);
            actions.push_back(action_tensor);
            rewards.push_back(torch::tensor({reward}, torch::kFloat32));
            torch::Tensor next_state_tensor = torch::from_blob(next_state_floats.data(), {(int)next_state_floats.size()}, torch::kFloat32).clone();
            next_states.push_back(next_state_tensor);
            dones.push_back(torch::tensor({done}, torch::kInt64));
        }

        // Stack tensors to form batches
        torch::Tensor state_batch = torch::stack(states);
        torch::Tensor action_batch = torch::stack(actions);
        torch::Tensor reward_batch = torch::stack(rewards).squeeze();
        torch::Tensor next_state_batch = torch::stack(next_states);
        torch::Tensor done_batch = torch::stack(dones).squeeze();

        // Update critic
        critic->train();
        torch::Tensor next_actions = target_actor->forward(next_state_batch);
        torch::Tensor next_Q_values = target_critic->forward(next_state_batch, next_actions);
        torch::Tensor expected_Q = reward_batch + gamma * next_Q_values * (1 - done_batch);
        torch::Tensor current_Q = critic->forward(state_batch, action_batch);
        torch::Tensor critic_loss = torch::mse_loss(current_Q, expected_Q);

        try{
            critic_optimizer->zero_grad();
            critic_loss.backward();
            critic_optimizer->step();

            // Update actor
            actor->train();
            torch::Tensor current_actions = actor->forward(state_batch);
            torch::Tensor actor_loss = -critic->forward(state_batch, current_actions).mean();

            actor_optimizer->zero_grad();
            actor_loss.backward();
            actor_optimizer->step();

            // Soft update target networks
            update_target_network(tau);
        } catch (const std::exception &e) {
            std::cerr << "Error in training: " << e.what() << std::endl;
        }
    }

    std::vector<float> convertToFloat(const std::vector<double>& input) {
        std::vector<float> output(input.size());
        std::transform(input.begin(), input.end(), output.begin(), [](double val) {
            return static_cast<float>(val);
        });
        return output;
    }

    void save_model() {
        std::string directory = "/groupstorage/group01/DDPGStrategy/models/";
        check_and_create_directory(directory); // Ensure the directory exists or create it

        std::string actor_model_path = directory + instrument_name + "_actor.pt";
        std::string actor_optimizer_path = directory + instrument_name + "_actor_optimizer.pt";
        std::string critic_model_path = directory + instrument_name + "_critic.pt";
        std::string critic_optimizer_path = directory + instrument_name + "_critic_optimizer.pt";

        try {
            // Save the model
            torch::save(actor, actor_model_path);
            std::cout << "Model saved to " << actor_model_path << std::endl;

            // Save the optimizer
            torch::save(*actor_optimizer, actor_optimizer_path);
            std::cout << "Optimizer saved to " << actor_optimizer_path << std::endl;

            // Save the model
            torch::save(critic, critic_model_path);
            std::cout << "Model saved to " << critic_model_path << std::endl;

            // Save the optimizer
            torch::save(*critic_optimizer, critic_optimizer_path);
            std::cout << "Optimizer saved to " << critic_optimizer_path << std::endl;

        } catch (const std::exception& e) {
            std::cerr << "Error saving model or optimizer: " << e.what() << std::endl;
        }
    }

    void load_model() {
        std::string directory = "/groupstorage/group01/DDPGStrategy/models/";

        std::string actor_model_path = directory + instrument_name + "_actor.pt";
        std::string actor_optimizer_path = directory + instrument_name + "_actor_optimizer.pt";
        std::string critic_model_path = directory + instrument_name + "_critic.pt";
        std::string critic_optimizer_path = directory + instrument_name + "_critic_optimizer.pt";

        try {
            if (check_model_exists(actor_model_path)) {
                torch::load(actor, actor_model_path);
                std::cout << "Model loaded successfully." << std::endl;
            } else {
                std::cerr << "Model file not found." << std::endl;
                return;
            }

            if (check_model_exists(actor_optimizer_path)) {
                torch::load(*actor_optimizer, actor_optimizer_path);
                std::cout << "Optimizer loaded successfully." << std::endl;
            } else {
                std::cerr << "Optimizer file not found." << std::endl;
                // Handle optimizer not found - you may want to recreate the optimizer
                initialize_optimizer(); // Optional
            }

            if (check_model_exists(critic_model_path)) {
                torch::load(critic, critic_model_path);
                std::cout << "Model loaded successfully." << std::endl;
            } else {
                std::cerr << "Model file not found." << std::endl;
                return;
            }

            if (check_model_exists(critic_optimizer_path)) {
                torch::load(*critic_optimizer, critic_optimizer_path);
                std::cout << "Optimizer loaded successfully." << std::endl;
            } else {
                std::cerr << "Optimizer file not found." << std::endl;
                // Handle optimizer not found - you may want to recreate the optimizer
                initialize_optimizer(); // Optional
            }

        } catch (const std::exception& e) {
            std::cerr << "Error loading model or optimizer: " << e.what() << std::endl;
            // Handle error and potentially recreate optimizer
        }
    }

    void check_and_create_directory(const std::string& path) {
        std::filesystem::path dir_path(path);

        // Check if the directory exists
        if (std::filesystem::exists(dir_path)) {
            std::cout << "Directory already exists: " << path << std::endl;
        } else {
            // Create the directory
            if (std::filesystem::create_directories(dir_path)) {
                std::cout << "Directory created: " << path << std::endl;
            } else {
                std::cerr << "Failed to create directory: " << path << std::endl;
            }
        }
    }

    bool check_model_exists(std::string file_name){
        std::filesystem::path model_path(file_name);
        return std::filesystem::exists(model_path);
    }

    void print_model_summary(const torch::nn::Module& model) {
        std::cout << "Model Summary:\n";
        std::cout << "---------------------------------\n";

        for (const auto& pair : model.named_parameters()) {
            const std::string& name = pair.key();
            const torch::Tensor& param = pair.value();
            std::cout << name << ": " << param.sizes() << "\n";
        }
        std::cout << "---------------------------------\n";
    }

    void print_agent_model_summary() {
        print_model_summary(*actor);
        print_model_summary(*critic);
    }
};