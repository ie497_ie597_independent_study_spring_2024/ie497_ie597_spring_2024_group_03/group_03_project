#include <torch/torch.h>

// Define a new Module.
struct Actor : torch::nn::Module {
    // Modify the constructor to accept input and output sizes.
    Actor(int state_size, int action_size) {
        // Construct and register Linear submodules with dynamic sizes.
        fc1 = register_module("fc1", torch::nn::Linear(state_size, 256));
        fc2 = register_module("fc2", torch::nn::Linear(256, 64));
        fc3 = register_module("fc3", torch::nn::Linear(64, action_size));

        // Initialize all non-bias weights with Glorot uniform.
        init_weights();
    }

    // Implement the Net's algorithm.
    torch::Tensor forward(torch::Tensor x) {
        x = torch::relu(fc1->forward(x.reshape({x.size(0), -1})));
        x = torch::dropout(x, /*p=*/0.1, /*train=*/is_training());
        x = torch::relu(fc2->forward(x));
        x = torch::dropout(x, /*p=*/0.1, /*train=*/is_training());
        x = torch::tanh(fc3->forward(x));  // Assuming this is the action output layer
        return x;
    }

    // apply glorot initialisation
    void init_weights() {
        auto parameters = named_parameters();
        for (auto& p : parameters) {
            if (p.key().find("bias") != std::string::npos) {
                p.value().data().zero_();
            } else if (p.key().find("weight") != std::string::npos) {
                auto fan_in = p.value().size(1);
                auto fan_out = p.value().size(0);
                double std = std::sqrt(6. / (fan_in + fan_out));
                p.value().data().uniform_(-std, std);
            }
        }
    }

    // Module subcomponents are declared here.
    torch::nn::Linear fc1{nullptr}, fc2{nullptr}, fc3{nullptr};
};

struct Critic : torch::nn::Module {
    // Modify the constructor to accept input and output sizes.
    Critic(int state_size, int action_size) {
        // Construct and register Linear submodules with dynamic sizes.
        int input_size = state_size + action_size;
        fc1 = register_module("fc1", torch::nn::Linear(input_size, 256));
        fc2 = register_module("fc2", torch::nn::Linear(256, 64));
        fc3 = register_module("fc3", torch::nn::Linear(64, 1));

        // Initialize all non-bias weights with Glorot uniform.
        init_weights();
    }

    // Implement the Net's algorithm.
    torch::Tensor forward(torch::Tensor state, torch::Tensor action) {
        // state and action are concatenated in the first layer
        torch::Tensor x = torch::cat({state, action}, 1);
        x = torch::relu(fc1->forward(x.reshape({x.size(0), -1})));
        x = torch::dropout(x, /*p=*/0.1, /*train=*/is_training());
        x = torch::relu(fc2->forward(x));
        x = torch::dropout(x, /*p=*/0.1, /*train=*/is_training());
        x = fc3->forward(x);  // Assuming this is the output layer (Q-value)
        return x;
    }
    
    // apply glorot initialisation
    void init_weights() {
        auto parameters = named_parameters();
        for (auto& p : parameters) {
            if (p.key().find("bias") != std::string::npos) {
                p.value().data().zero_();
            } else if (p.key().find("weight") != std::string::npos) {
                auto fan_in = p.value().size(1);
                auto fan_out = p.value().size(0);
                double std = std::sqrt(6. / (fan_in + fan_out));
                p.value().data().uniform_(-std, std);
            }
        }
    }

    // Module subcomponents are declared here.
    torch::nn::Linear fc1{nullptr}, fc2{nullptr}, fc3{nullptr};
};