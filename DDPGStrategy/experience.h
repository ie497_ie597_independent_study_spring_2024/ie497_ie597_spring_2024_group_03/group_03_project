// Experience.h
#ifndef EXPERIENCE_H
#define EXPERIENCE_H

#include <vector>
#include <tuple>

using Experience = std::tuple<std::vector<double>, std::pair<double,double> , float, std::vector<double>, int>;

#endif // EXPERIENCE_H