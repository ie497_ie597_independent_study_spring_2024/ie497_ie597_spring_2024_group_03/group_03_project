#!/bin/bash

jupyter-nbconvert --TagRemovePreprocessor.enabled=True --TagRemovePreprocessor.remove_cell_tags={\"remove\"} --to python your_strategy.ipynb --output ../MyStrategy.py
cd ..

mkdir -p ~/ss/bt/strategies_dlls

rm ~/ss/bt/strategies_dlls/PybindStrategy.so

make copy_strategy

cd ~/ss/bt/ ; ./StrategyServerBacktesting &
sleep 1

startDate='2021-11-05'
endDate='2021-11-05'
instanceName='TestPybindStrategy'

cd ~/ss/bt/utilities
./StrategyCommandLine cmd create_instance $instanceName PybindStrategy UIUC SIM-1001-101 dlariviere 1000000 -symbols "AAPL"
./StrategyCommandLine cmd strategy_instance_list

sleep 1

# Start the backtest
./StrategyCommandLine cmd start_backtest $startDate $endDate $instanceName 0


# Get the line number which ends with finished. 
foundFinishedLogFile=$(grep -nr "finished.$" ~/ss/bt/logs/main_log.txt | gawk '{print $1}' FS=":"|tail -1)

# DEBUGGING OUTPUT
echo "Last line found:",$foundFinishedLogFile

# If the line ending with finished. is less than the previous length of the log file, then strategyBacktesting has not finished, 
# once its greater than the previous, it means it has finished.
while ((logFileNumLines > foundFinishedLogFile))
do
    foundFinishedLogFile=$(grep -nr "finished.$" ~/ss/bt/logs/main_log.txt | gawk '{print $1}' FS=":"|tail -1)

    #DEBUGGING OUTPUT
    echo "Waiting for strategy to finish"
    sleep 1
done

echo "Sleeping for 10 seconds..."

sleep 10

echo "run_backtest.sh: Strategy Studio finished backtesting"

latestCRA=$(ls ~/ss/bt/backtesting-results/BACK_*.cra -t | head -n1)

# Print the path of the craFile
echo "CRA file found:", $latestCRA

# Use the StrategyCommandLine utility to eport the CRA file to CSV format
# Name of file should be the latest craFile created
./StrategyCommandLine cmd export_cra_file $latestCRA ~/ss/bt/backtesting-results/csv_files

rm ~/ss/sdk/RCM/StrategyStudio/examples/strategies/PybindStrategy/VisualizationScript/data/fills.csv
rm ~/ss/sdk/RCM/StrategyStudio/examples/strategies/PybindStrategy/VisualizationScript/data/pnl.csv
rm ~/ss/sdk/RCM/StrategyStudio/examples/strategies/PybindStrategy/VisualizationScript/data/order.csv

mv `ls -t ~/ss/bt/backtesting-results/csv_files/BACK_*_fill.csv | head -n1` ~/ss/sdk/RCM/StrategyStudio/examples/strategies/PybindStrategy/VisualizationScript/data/fills.csv
mv `ls -t ~/ss/bt/backtesting-results/csv_files/BACK_*_pnl.csv | head -n1`  ~/ss/sdk/RCM/StrategyStudio/examples/strategies/PybindStrategy/VisualizationScript/data/pnl.csv
mv `ls -t ~/ss/bt/backtesting-results/csv_files/BACK_*_order.csv | head -n1`  ~/ss/sdk/RCM/StrategyStudio/examples/strategies/PybindStrategy/VisualizationScript/data/order.csv
